//
//  UIColors.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

struct UIColors {
    static let darkerBlue = UIColor(red: 14.0 / 255.0, green: 62.0 / 255.0, blue: 123.0 / 255.0, alpha: 1.0)
    static let darkBlue = UIColor(red: 16.0 / 255.0, green: 70.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    static let lightBlue = UIColor(red: 16.0 / 255.0, green: 70.0 / 255.0, blue: 140.0 / 255.0, alpha: 0.37)
    static let lightGray = UIColor(red: 226.0 / 255.0, green: 226.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
    static let yellow = UIColor(hexString: "#FFC000")
    static let green = UIColor(hexString: "#6dae59")
    static let darkGray = UIColor(hexString: "FF323232")
}
