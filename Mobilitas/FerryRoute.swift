//
//  FerryRoute.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 01/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

enum Route {
    struct Response: Codable {
        var id: Int = 0
        var title: String = ""
    }
}

enum FerryRoute {
    struct Price: Codable {
        var pricing_type: String
        var contents: String
        var price: String
    }

    struct Period: Codable {
        var dates: String
        var destinations: [Destination]
    }

    struct Destination: Codable {
        var departure: String
        var daily: [String]
        var sundays: [String]
    }

    struct Request: Codable {
        var id: Int
    }

    struct Response: Codable {
        var id: Int = 0
        var title: String = ""
        var winter: Period
        var summer: Period
        var prices: [Price] = []
    }
}

enum AirportTaxi {
    struct Request: Codable { }
    struct Response: Codable {
        var destination: String = ""
        var price: String = ""
    }
}

enum CycleHire {
    struct Request: Codable { }
    struct Response: Codable {
        var id: Int
        var title: String
        var link: String
    }
}

enum BikeInfo {
    struct Request: Codable { }
    struct Response: Codable {
        var account_type: AccountType
    }

    struct AccountType: Codable {
        var account_type: String
        var priceswrapper: [TimePrice]
    }

    struct TimePrice: Codable {
        var time: String
        var price: String
    }
}

enum BikeStation {
    struct Request: Codable { }
    struct Response: Codable {
        var id: Int
        var title: String
        var small_description: String?
        var map: MapInfo
    }

    struct MapInfo: Codable {
        var address: String
        var lat: Double
        var lng: Double
        var zoom: Int
        var place_id: String
        var city: String
        var country: String
        var country_short: String
    }
}

enum GoogleMapsDestination {
    struct Request: Codable { }
    struct Response: Codable {
        var routes: [Route]
        var status: String

        struct Route: Codable {
            var bounds: Bound
            var legs: [GeneralRouteInfo]
        }

        struct Bound: Codable {
            var northeast: Location
            var southwest: Location
        }

        struct Location: Codable {
            var lat: Double
            var lng: Double
        }

        struct GeneralRouteInfo: Codable {
            var arrival_time: Time?
            var departure_time: Time?
            var distance: TextValueType
            var duration: TextValueType
            var end_location: Location
            var start_address: String
            var start_location: Location
            var end_address: String
            var steps: [Step]
        }

        struct Time: Codable {
            var text: String
            var time_zone: String
            var value: Int
        }

        struct TextValueType: Codable {
            var text: String
            var value: Int
        }

        struct Step: Codable {
            var distance: TextValueType
            var duration: TextValueType
            var end_location: Location
            var html_instructions: String?
            var start_location: Location
            var transit_details: TransitDetails?
            var travel_mode: String
            var polyline: Polyline?
            var steps: [Step]?
        }

        struct Polyline: Codable {
            var points: String?
        }

        struct TransitDetails: Codable {
            var arrival_stop: LocationWithName
            var arrival_time: Time?
            var departure_stop: LocationWithName
            var departure_time: Time?
            var headsign: String
            var line: Line
            var num_stops: Int
        }

        struct Line: Codable {
            var agencies: [Agency]
            var color: String
            var name: String
            var short_name: String
            var text_color: String
            var vehicle: Vehicle
        }

        struct Vehicle: Codable {
            var icon: String
            var name: String
            var type: String
        }

        struct Agency: Codable {
            var name: String
            var phone: String?
            var url: String
        }

        struct LocationWithName: Codable {
            var location: Location
            var name: String
        }
    }
}

enum StaticText {
    struct Response: Codable {
        var about_us_text: String
        var bike_saving: String
        var bike_text: String
        var contact_text: String
        var cycle_saving: String
        var cycle_text: String
        var electric_car_saving: String
        var ferry_text: String
        var info_text: String
        var taxi_text: String
    }
}

struct LocationInfo {
    var name = "Your Location"
    var latitude: Double
    var longitude: Double
}
