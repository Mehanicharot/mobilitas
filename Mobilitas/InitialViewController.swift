//
//  InitialViewController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 17/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit
import CoreLocation
import NVActivityIndicatorView

class InitialViewController: UIViewController {
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        loadingView.startAnimating()

        WebAPIController.shared.ferryRoutesRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let routes):
                    ActiveUser.shared.ferryRoutes = routes
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.airportTaxiRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let airportTaxi):
                    ActiveUser.shared.airportTaxi = airportTaxi
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.cycleHireRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let cycleHire):
                    ActiveUser.shared.cycleHire = cycleHire
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.bikeInfoRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let bikeInfo):
                    ActiveUser.shared.bikeInfo = bikeInfo
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.bikeStationsRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let bikeStations):
                    ActiveUser.shared.bikeStations = bikeStations
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.ferryTaxiRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let ferryTaxi):
                    ActiveUser.shared.ferryTaxi = ferryTaxi
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.electricCarRequest(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let electricCar):
                    ActiveUser.shared.electricCar = electricCar
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })

        WebAPIController.shared.getText(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let staticText):
                    ActiveUser.shared.staticText = staticText
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.checkLocationStatus()
        })
    }

    private func checkLocationStatus() {
//        let status = CLLocationManager.authorizationStatus()
//
//        switch status {
//        case .notDetermined:
//            //locationManager.requestWhenInUseAuthorization()
//            return
//        case .denied, .restricted:
//            return
//        case .authorizedAlways, .authorizedWhenInUse:
//            nextStep()
//
//        @unknown default:
//            fatalError()
//        }

        // 4
        nextStep()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }

    private func nextStep() {
        if let username = ActiveUser.shared.username, let password = ActiveUser.shared.password {
            let data = Login.Request(username: username, password: password)
            WebAPIController.shared.loginRequest(data, completion: { [weak self] results in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    switch results {
                    case .success(let newUser):
                        guard let user = newUser else { return }
                        ActiveUser.shared.setBasicUserData(user)
                        let userDataRequest = UserData.Request(userId: user.userId)
                        WebAPIController.shared.userDataRequest(userDataRequest, completion: {[weak self] results in
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else { return }
                                switch results {
                                case .success(let newUserInfo):
                                    guard let userInfo = newUserInfo else { return }
                                    ActiveUser.shared.setUserData(userInfo)
                                    self.performSegue(withIdentifier: "Home", sender: nil)
                                case .failure(let error):
                                    print(error)
                                    WebAPIController.shared.userDataRequest2(userDataRequest, completion: { [weak self] results in
                                    DispatchQueue.main.async { [weak self] in
                                        guard let self = self else { return }
                                        switch results {
                                        case .success(let newUserInfo2):
                                            if let userInfo = newUserInfo2 {
                                                let user = UserData.Response(id: userInfo.id,
                                                                             first_name: userInfo.first_name,
                                                                             last_name: userInfo.last_name,
                                                                             email: userInfo.email,
                                                                             address_1: userInfo.address_1,
                                                                             address_2: userInfo.address_2,
                                                                             country: userInfo.country,
                                                                             birthday: userInfo.birthday,
                                                                             phone_number: userInfo.phone_number,
                                                                             saved_locations: [],
                                                                             facts: userInfo.facts)
                                                ActiveUser.shared.setUserData(user)
                                                self.performSegue(withIdentifier: "Home", sender: nil)
                                            }
                                        case .failure(let error):
                                            ActiveUser.shared.removeStorageData()
                                            AlertManager.shared.showAlertWtihSegue(self, segueIdentifier: "MainViewController", message: error.localizedDescription)
                                        }
                                    }

                                    })

                                }
                            }
                        })
                    case .failure(let error):
                        ActiveUser.shared.removeStorageData()
                        AlertManager.shared.showAlertWtihSegue(self, segueIdentifier: "MainViewController", message: error.localizedDescription)
                    }
                }
            })
        } else {
            performSegue(withIdentifier: "MainViewController", sender: nil)
        }
    }
}

extension InitialViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        ActiveUser.shared.myLocation = LocationInfo(name: "Your Location", latitude: locValue.latitude, longitude: locValue.longitude)
    }
}
