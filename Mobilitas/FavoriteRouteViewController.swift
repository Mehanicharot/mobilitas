//
//  FavoriteRouteViewController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 23/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol FavoriteLocationDelegate {
    func cancelPressed(_ view: FavoriteRouteViewController)
    func didPickName(_ view: FavoriteRouteViewController, name: String)
}

class FavoriteRouteViewController: UIViewController {

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var routeName: BlueLineUITextField!
    @IBOutlet weak var containerView: UIView!

    var delegate: FavoriteLocationDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.layer.borderColor = UIColors.darkBlue.cgColor
        containerView.layer.borderWidth = 2

    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        delegate?.cancelPressed(self)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func donePressed(_ sender: Any) {
        if routeName.text != "" {
            delegate?.didPickName(self, name: routeName.text ?? "location")
            self.dismiss(animated: true, completion: nil)
        }
    }
}
