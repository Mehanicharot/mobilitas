//
//  StepView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 09/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class StepView: UIView {
    @IBOutlet weak var stackContainer: UIStackView!

    func prepare(with steps: [GoogleMapsDestination.Response.Step], isLast: Bool) {
        for (index, step) in steps.enumerated() {
            let stepPartView: StepPartView = .fromNib()
            stepPartView.prepare(with: step, isLast: isLast && index == steps.count-1)

            stackContainer.addArrangedSubview(stepPartView)
            stackContainer.alignment = .fill
            stackContainer.distribution = .equalSpacing
        }

    }
}
