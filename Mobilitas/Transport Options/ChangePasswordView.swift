//
//  ChangePasswordView.swift
//  
//
//  Created by Martin . Andonovski on 25/02/2020.
//

import UIKit

protocol ChangePasswordDelegate {
    func didTapForOpen(_ view: ChangePasswordView)
    func didSaveNewPassword()
}

class ChangePasswordView: UIView {
    var delegate: ChangePasswordDelegate?

    @IBAction func didTapOnChangeButton(_ sender: Any) {
        delegate?.didTapForOpen(self)
    }
}
