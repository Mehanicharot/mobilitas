//
//  ProfileInfoView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 20/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class ProfileInfoView: UIView {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var totalTravelStaticTextLabel: UILabel!
    @IBOutlet weak var totalWalkStaticTextLabel: UILabel!
    @IBOutlet weak var totalSavedStaticTextLabel: UILabel!
    @IBOutlet weak var totalTravelLabel: UILabel!
    @IBOutlet weak var walkedDistanceLabel: UILabel!
    @IBOutlet weak var youSavedLabel: UILabel!

    @IBOutlet weak var info1Stack: UIStackView!
    @IBOutlet weak var info2Stack: UIStackView!

    func prepare(with data: UserData.Facts) {
        info1Stack.isHidden = data.status == 0
        info2Stack.isHidden = data.status == 0
        messageLabel.isHidden = data.status == 1

        if data.status == 1 {
            totalTravelStaticTextLabel.text = data.line_bus_distance
            totalWalkStaticTextLabel.text = data.line_walking_distance
            totalSavedStaticTextLabel.text = data.line_walking_distance
            totalTravelLabel.text = data.bus_distance
            walkedDistanceLabel.text = data.walking_distance
            youSavedLabel.text = data.total_saved
        } else if data.status == 0 {
            messageLabel.text = data.message
        }


    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
