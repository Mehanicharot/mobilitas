//
//  BikeStationInfoView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 06/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class BikeStationInfoView: UIView {
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var pointImageView: UIImageView!

    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }

    func prepare(with title: String) {
        self.stationNameLabel.text = title
    }

    func prepare(with destiantion1: String, destination2: String, shouldUseYellowImage: Bool = true) {
        stationNameLabel.text = destiantion1
        secondLabel.text = destination2
        lineView.isHidden = false
        secondStackView.isHidden = false
        pointImageView.image = shouldUseYellowImage ? UIImage(named: "circle_green") : UIImage(named: "circle")
        drawDots()
    }

    private func drawDots() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = line.frame.width
        shapeLayer.lineDashPattern = [3, 1.5] // 7 is the length of dash, 3 is length of the gap.

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0), CGPoint(x: 0, y: line.frame.height)])
        shapeLayer.path = path
        line.layer.addSublayer(shapeLayer)
    }
}
