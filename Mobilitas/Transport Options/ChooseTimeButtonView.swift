//
//  ChooseTimeButtonView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 13/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol TimeButtonDelegate {
    func didTapOnNowButton(_ view: ChooseTimeButtonView)
    func didTapOnChooseTimeButton(_ view: ChooseTimeButtonView)
}

class ChooseTimeButtonView: UIView {

    @IBOutlet weak var nowButton: UIButton!
    @IBOutlet weak var chooseTimeButton: UIButton!

    var delegate: TimeButtonDelegate?

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        selectedButton(nowButton)
        unselectedButton(chooseTimeButton)
    }

    @IBAction func nowPressed(_ sender: Any) {
        selectedButton(nowButton)
        unselectedButton(chooseTimeButton)
        chooseTimeButton.setTitle("Choose Time", for: .normal)
        delegate?.didTapOnNowButton(self)
    }

    @IBAction func chooseTimePressed(_ sender: Any) {
        selectedButton(chooseTimeButton)
        unselectedButton(nowButton)
        delegate?.didTapOnChooseTimeButton(self)
    }

    private func selectedButton(_ button: UIButton) {
        button.layer.borderColor = UIColors.darkBlue.cgColor
        button.layer.borderWidth = 2
        button.setTitleColor(UIColors.darkBlue, for: .normal)
    }

    private func unselectedButton(_ button: UIButton) {
        button.layer.borderColor = UIColors.lightGray.cgColor
        button.layer.borderWidth = 1
        button.setTitleColor(UIColor.gray, for: .normal)
    }
}
