//
//  ToLabelView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 21/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol SwitchDirectionsDelegate {
    func switchDirections(_ view: ToLabelView)
}

class ToLabelView: UIView {

    var delegate: SwitchDirectionsDelegate?

    @IBAction func switchDirectionPressed(_ sender: Any) {
        delegate?.switchDirections(self)
    }
}
