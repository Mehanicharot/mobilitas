//
//  BasicCellWithArrow.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 31/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

enum MainCategories {
    case direction
    case transportOption
    case account
}

protocol BasicCellDelegate {
    func didTapOnNextButton(_ view: BasicCellWithArrow, type: TransportOptionSteps)
    func didTapOnNextButton(_ view: BasicCellWithArrow, type: AccountSteps)
    func didTapOnNextButton(_ view: BasicCellWithArrow)
}

protocol VisitLocationDelegate {
    func didPressOnCheckLocation(_ view: BasicCellWithArrow, location: UserData.Response.Location?)
}

class BasicCellWithArrow: UIView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainDirectionStackView: UIStackView!
    @IBOutlet weak var stepsContainer: UIView!
    @IBOutlet weak var arrowButton: UIButton!

    var route: Route.Response?
    var cycleHire: CycleHire.Response?
    var bikeSationDetails: BikeStation.Response?
    var buttonType: TransportOptionSteps!
    var accountType: AccountSteps!
    var delegate: BasicCellDelegate?
    var visitDelegate: VisitLocationDelegate?
    var leg: GoogleMapsDestination.Response.GeneralRouteInfo?
    var location: UserData.Response.Location?

    private var numberOfColumns: Int = 3

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupBorder()
    }

    func prepareView(for type: TransportOptionSteps, route: Route.Response? = nil) {
        buttonType = type
        switch type {
        case .ferry:
            title.text = "Ferry Schedule"
            subtitle.text = "View Malta’s ferry schedule"
            image.image = UIImage(named: "boat")
        case .taxi:
            title.text = "Airport Taxi"
            subtitle.text = "Browse Airport taxi options"
            image.image = UIImage(named: "taxi")
        case .bike:
            title.text = "Bike Rentals"
            subtitle.text = "Rent a bike - rentals and prices"
            image.image = UIImage(named: "bike")
        case .cycle:
            title.text = "Cycle Hire"
            subtitle.text = "Rent a bycicle"
            image.image = UIImage(named: "cycle")
        case .inFerry:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = route?.title ?? ""
        case .inTaxi, .inBike, .inCycle: break
        case .inBikeStations:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "BIKE STATIONS"
        case .inBikePricingInfo:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "PRICING INFO"
        case .inFerryWinnter:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "WINNTER"
        case .inFerrySummer:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "SUMMER"
        case .inFerryPrices:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "PRICES"
        case .main: break
        case .inFerryData:
            print("++++++++++++++++")
        case .inBikeStationDetails:
            break
        case .ferryTaxi:
            title.text = "Ferry Water Taxi"
            subtitle.text = "View Malta’s Ferry Water Taxi"
            image.image = UIImage(named: "ferry-water-taxi")
        case .electricCar:
            title.text = "Electric Car"
            subtitle.text = "View Malta’s Electric Car"
            image.image = UIImage(named: "electric-car")
        case .inElectricCar:
            break
        case .inSavedLocation:
            break
        }
    }

    func prepareView(title: String, bikeStation: BikeStation.Response) {
        bikeSationDetails = bikeStation
        subtitle.isHidden = true
        image.isHidden = true
        self.title.text = title
    }

    func prepareView(for location: UserData.Response.Location) {
        subtitle.isHidden = true
        image.isHidden = true
        title.text = location.title
        self.location = location
        arrowButton.setTitle("x", for: .normal)
        arrowButton.setImage(nil, for: .normal)
    }

    func prepareView(for type: AccountSteps, info: Route.Response? = nil) {
        accountType = type
        switch type {
        case .edit:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "EDIT PERSONAL INFO"
        case .main: break
        case .savedLocations:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "VIEW SAVED LOCATIONS"
        case .login:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "LOGIN"
        case .register:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = "REGISTER"
        case .changePassword:
            break
        case .savedLocationDetails:
            break
        }
    }

    func prepareView(for type: TransportOptionSteps, cycleHire: CycleHire.Response) {
        buttonType = type
        self.cycleHire = cycleHire
        switch type {
        case .inCycle, .ferryTaxi, .electricCar:
            subtitle.isHidden = true
            image.isHidden = true
            title.text = cycleHire.title
            arrowButton.setImage(UIImage(named: "link"), for: .normal)
        default: break
        }
    }

    func prepareView(for route: GoogleMapsDestination.Response.Route) {
        if route.legs.count <= 0 { return }
        self.title.text = "\(route.legs[0].departure_time?.text ?? "") - \(route.legs[0].arrival_time?.text ?? "")"
        self.subtitle.text = "Duration: \(route.legs[0].duration.text)"
        self.image.isHidden = true
        self.mainDirectionStackView.isHidden = false
        self.stepsContainer.isHidden = false
        let steps = refactoreSteps(route.legs[0].steps)


        for (index, step) in steps.enumerated() {
            let stepView: StepView = .fromNib()
            stepView.prepare(with: step, isLast: index == steps.count-1)
            stepView.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
            mainDirectionStackView.addArrangedSubview(stepView)
        }

        self.leg = route.legs.first
    }

    private func refactoreSteps(_ steps: [GoogleMapsDestination.Response.Step]) -> [[GoogleMapsDestination.Response.Step]] {
        let numberInLine = 3
        var finalStepsArray: [[GoogleMapsDestination.Response.Step]] = []
        var arraySteps: [GoogleMapsDestination.Response.Step] = []


        for (index, step) in steps.enumerated() {
            arraySteps.append(step)
            if (index+1) % numberInLine == 0 {
                finalStepsArray.append(arraySteps)
                arraySteps = []
            }
        }

        if arraySteps.count != 0 {
            finalStepsArray.append(arraySteps)
        }

        return finalStepsArray
    }

    private func append(views: [UIView], on stackView: UIStackView) {
        var newViews = views
        let restToAdd = numberOfColumns - views.count

        if restToAdd != 0 {
            for _ in  0...restToAdd - 1 {
                let newLabel = UILabel()
                newLabel.text = "   "
                newLabel.textAlignment = .right
                newViews.append(newLabel)
            }
        }

        let newStackView = UIStackView(arrangedSubviews: newViews)
        newStackView.axis = .horizontal
        newStackView.distribution = .fillEqually
        newStackView.alignment = .fill
        stackView.addArrangedSubview(newStackView)
    }

    private func setupBorder() {
        containerView.layer.masksToBounds = false
        containerView.layer.borderColor = UIColors.lightGray.cgColor
        containerView.layer.borderWidth = 1.2
    }

    @IBAction func didTapOnRemoveLocation(_ sender: Any) {
        visitDelegate?.didPressOnCheckLocation(self, location: location)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        if let transport = buttonType {
            delegate?.didTapOnNextButton(self, type: transport)
        } else if let account = accountType {
            delegate?.didTapOnNextButton(self, type: account)
        } else {
            delegate?.didTapOnNextButton(self)
        }
    }
}
