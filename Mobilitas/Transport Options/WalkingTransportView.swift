//
//  WalkingTransportView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 14/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol WalkingViewDelegate {
    func didTapOnWalk(_ view: WalkingTransportView, step: GoogleMapsDestination.Response.Step?)
    func didTapOnLocation(_ view: WalkingTransportView, step: GoogleMapsDestination.Response.Step?, from: String, to: String)
}

class WalkingTransportView: UIView {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var subDepartureLocationLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var stepsStackView: UIStackView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var bottomCircleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topCircleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var walkingFromDesstinationNameLabel: UILabel!
    @IBOutlet weak var walkingToDestinationLabel: UILabel!

    private var stepsInfo: [UILabel] = []
    private var step: GoogleMapsDestination.Response.Step?
    private var walkingFrom: String!
    private var walkingTo: String!


    var delegate: WalkingViewDelegate?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupCircleView()
    }

    func prepare(with step: GoogleMapsDestination.Response.Step, shouldDisplayTopCircle: Bool, shouldDisplayBottomCircle: Bool, from: String?, to: String?,  walkingFromTitle: String, wlakingToTitle: String) {
        durationLabel.text = step.duration.text
        topCircleHeightConstraint.constant = shouldDisplayTopCircle ? 20 : 0
        bottomCircleHeightConstraint.constant = shouldDisplayBottomCircle ? 20 : 0
        topConstraint.constant = shouldDisplayTopCircle ? 15 : 0
        bottomConstraint.constant = shouldDisplayBottomCircle ? 15 : 0

        walkingFromDesstinationNameLabel.isHidden = !shouldDisplayTopCircle
        walkingToDestinationLabel.isHidden = !shouldDisplayBottomCircle
        walkingFromDesstinationNameLabel.text = from
        walkingToDestinationLabel.text = to

        walkingFrom = walkingFromTitle
        walkingTo = wlakingToTitle

        self.step = step
        guard let steps = step.steps else { return }
        for step in steps {
            let label = UILabel()
            label.font = UIFont(descriptor: UIFontDescriptor(name: "Futura", size: 13), size: 13)
            label.attributedText = step.html_instructions?.htmlToAttributedString
            label.numberOfLines = 0
            if label.text != "" {
                stepsStackView.addArrangedSubview(label)
                stepsInfo.append(label)
                label.isHidden = true
            }
        }

    }

    @IBAction func seeMoreDetailsPressed(_ sender: Any) {
        for label in stepsInfo {
            label.isHidden = !label.isHidden
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.setupCircleView()
        })
        self.delegate?.didTapOnWalk(self, step: self.step)
    }

    @IBAction func walkLocationPressed(_ sender: Any) {
        delegate?.didTapOnLocation(self, step: step, from: walkingFrom, to: walkingTo)

    }

    private func setupCircleView() {
        if let sublayers = circleView.layer.sublayers {
            for layer in sublayers {
                layer.removeFromSuperlayer()
            }
        }
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColors.darkBlue.cgColor
        shapeLayer.lineWidth = circleView.frame.width
        shapeLayer.lineCap = .round
        shapeLayer.lineDashPattern = [1, 10] // 7 is the length of dash, 3 is length of the gap.

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 2.5, y: 0), CGPoint(x: 2.5, y: circleView.frame.height)])
        shapeLayer.path = path
        circleView.layer.addSublayer(shapeLayer)
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
