//
//  StepPartView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 09/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class StepPartView: UIView {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var numberContainer: UIView!
    @IBOutlet weak var arrow: UIImageView!

    func prepare(with step: GoogleMapsDestination.Response.Step, isLast: Bool) {

        if step.travel_mode == "TRANSIT" {
            image.image = UIImage(named: "bus")
            numberContainer.isHidden = false
            number.text = step.transit_details?.line.short_name ?? ""
        } else if step.travel_mode == "WALKING" {
            image.image = UIImage(named: "walk")
            numberContainer.isHidden = true
        } else {
            image.isHidden = true
        }

        arrow.isHidden = isLast
    }
}
