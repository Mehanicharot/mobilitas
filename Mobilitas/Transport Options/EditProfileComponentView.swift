//
//  EditProfileComponentView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 17/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol CalendarTextFieldDelegate {
    func didTap(_ view: EditProfileComponentView)
}

class EditProfileComponentView: UIView, UITextFieldDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!

    var delegate: CalendarTextFieldDelegate?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        valueTextField.delegate = self
    }

    func prepare(_ text: String, value: String) {
        titleLabel.text = text
        valueTextField.text = value
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if titleLabel.text == "Birthday:" {
            textField.resignFirstResponder()
            textField.inputView = UIView()
            delegate?.didTap(self)
        } else if titleLabel.text == "Country:" {
            textField.resignFirstResponder()
            textField.inputView = UIView()
            delegate?.didTap(self)
        }
    }
}
