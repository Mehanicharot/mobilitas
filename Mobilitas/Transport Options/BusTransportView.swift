//
//  BusTransportView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 14/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class BusTransportView: UIView {
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var departureTimeLabel: UILabel!
    @IBOutlet weak var arrivalTimeLabel: UILabel!
    @IBOutlet weak var busNumberLabel: UILabel!
    @IBOutlet weak var busStationLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var sideLineView: UIView!
    @IBOutlet weak var numberLineView: UIView!
    @IBOutlet weak var bottomCircleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topCircleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrivalLocationLabel: UILabel!

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }

    func prepare(with step: GoogleMapsDestination.Response.Step, shouldHideBottomCircel: Bool) {
        guard let transit = step.transit_details else { return }
        locationLabel.text = transit.departure_stop.name
        durationLabel.text = step.duration.text
        busNumberLabel.text = transit.line.short_name
        busNumberLabel.textColor = UIColor(hexString: transit.line.text_color)
        busStationLabel.text = transit.headsign
        sideLineView.backgroundColor = UIColor(hexString: transit.line.color)
        numberLineView.backgroundColor = UIColor(hexString: transit.line.color)
        departureTimeLabel.text = step.transit_details?.departure_time?.text
        arrivalTimeLabel.text = step.transit_details?.arrival_time?.text
        arrivalLocationLabel.text = step.transit_details?.arrival_stop.name

        if transit.line.color == "#ffffff" {
            numberLineView.layer.borderColor = UIColors.lightGray.cgColor
            numberLineView.layer.borderWidth = 1
            sideLineView.layer.borderWidth = 1
            sideLineView.layer.borderColor = UIColor.lightGray.cgColor
        }

        heightConstraint.constant = shouldHideBottomCircel ? 0 : 20
        bottomConstraint.constant = shouldHideBottomCircel ? 0 : 15
        arrivalTimeLabel.isHidden = shouldHideBottomCircel
        arrivalLocationLabel.isHidden = shouldHideBottomCircel
    }

}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
