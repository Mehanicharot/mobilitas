//
//  AirportTaxiCellView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 04/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class AirportTaxiCellView: UIView {
    @IBOutlet weak var headerStackView: UIStackView!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    func prepare(with taxi: AirportTaxi.Response, hideHeader: Bool) {
        headerStackView.isHidden = hideHeader
        destinationLabel.text = taxi.destination
        priceLabel.text = taxi.price
    }

}
