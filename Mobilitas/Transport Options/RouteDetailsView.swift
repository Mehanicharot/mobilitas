//
//  RouteDetailsView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 14/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol FavoriteRouteDelegate {
    func addToFavorite(_ view: RouteDetailsView, routeDetails: GoogleMapsDestination.Response.GeneralRouteInfo?)
}

class RouteDetailsView: UIView {
    @IBOutlet weak var timeFromToLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var walkingLabel: UILabel!
    @IBOutlet weak var totalDistanceLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var starIamge: UIImageView!

    private var info: GoogleMapsDestination.Response.GeneralRouteInfo?
    var delegate: FavoriteRouteDelegate?

    var transitDistance: Double = 0.0
    var walkingDistance: Double = 0.0

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        containerView.layer.borderColor = UIColors.lightGray.cgColor
        containerView.layer.borderWidth = 2
    }

    @IBAction func addFavoritePressed(_ sender: Any) {
        delegate?.addToFavorite(self, routeDetails: info)
    }

    func prepare(with routeDetails: GoogleMapsDestination.Response.GeneralRouteInfo){
        info = routeDetails
        var walkingDurationInMin = 0
        var transitDistance = 0.0
        var walkingDistance = 0.0
        for step in routeDetails.steps {
            if step.travel_mode == "WALKING" {
                let parts = step.duration.text.split(separator: " ")
                if parts.count >= 4 {
                    let hours = Int(parts[0]) ?? 0
                    let min = Int(parts[2]) ?? 0
                    walkingDurationInMin += ((hours * 60) + min)
                } else if parts.count >= 2 {
                    let min = Int(parts[0]) ?? 0
                    walkingDurationInMin += min
                }
                let partsNew = step.distance.text.split(separator: " ")
                let distance = Double(partsNew[0]) ?? 0.0
                walkingDistance += distance
            } else if step.travel_mode == "TRANSIT" {
                let parts = step.distance.text.split(separator: " ")
                let distance = Double(parts[0]) ?? 0.0
                transitDistance += distance
            }
        }

        let walkingDurationInHours = walkingDurationInMin / 60
        walkingDurationInMin -= walkingDurationInHours * 60

        self.transitDistance = transitDistance
        self.walkingDistance = walkingDistance

        var text = ""

        if walkingDurationInHours != 0 {
            text = "\(walkingDurationInHours) hours \(walkingDurationInMin) min"
        } else {
            text = "\(walkingDurationInMin) min"
        }

        timeFromToLabel.text = "\(routeDetails.departure_time?.text ?? "") - \(routeDetails.arrival_time?.text ?? "")"
        durationLabel.text = "\(routeDetails.duration.text)"
        walkingLabel.text = "\(text)"
        totalDistanceLabel.text = "\(routeDetails.distance.text)"
    }
}
