//
//  DropDownWithDetails.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 04/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol DropDownDelegate {
    func didTapOnMore(_ view: DropDownWithDetails)
}

class DropDownWithDetails: UIView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var dailyLabel: UILabel!
    @IBOutlet weak var sundayLabel: UILabel!
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var timeOneStackView: UIStackView!
    @IBOutlet weak var timeSecondStackView: UIStackView!
    @IBOutlet weak var buttonArrow: UIButton!

    private let numberOfColumns = 3

    var delegate: DropDownDelegate?

    var type: Int = 0

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        showMoreButton.isSelected = false
        setupElements()
        setupBorder()
    }

    func prepare(with destination: FerryRoute.Destination) {
        title.text = destination.departure

        var labels: [UILabel] = []
        for (index, daily) in destination.daily.enumerated() {
            let newLabel = UILabel()
            newLabel.text = "\(daily)"
            newLabel.textAlignment = .left
            labels.append(newLabel)

            if (index+1) % numberOfColumns == 0 {
                append(views: labels, on: timeOneStackView)
                labels = []
            }
        }

        append(views: labels, on: timeOneStackView)
        labels = []

        for (index, sunday) in destination.sundays.enumerated() {
            let newLabel = UILabel()
            newLabel.text = "\(sunday)"
            newLabel.textAlignment = .left
            labels.append(newLabel)

            if (index+1) % numberOfColumns == 0 {
                self.append(views: labels, on: timeSecondStackView)
                labels = []
            }
        }

        append(views: labels, on: timeSecondStackView)
        labels = []
    }

    func prepare(with price: FerryRoute.Price) {
        title.text = price.pricing_type

        let contentsLabel = UILabel()
        contentsLabel.text = price.contents
        contentsLabel.textAlignment = .left

        let priceLabel = UILabel()
        priceLabel.text = price.price
        priceLabel.textAlignment = .left

        let newStackView = UIStackView()
        newStackView.addArrangedSubview(contentsLabel)
        newStackView.addArrangedSubview(priceLabel)
        newStackView.axis = .horizontal
        newStackView.distribution = .fillEqually
        newStackView.alignment = .fill
        timeOneStackView.addArrangedSubview(newStackView)

        type = 1
        timeSecondStackView.isHidden = true
        dailyLabel.isHidden = true
        sundayLabel.isHidden = true
    }

    func prepare(with info: BikeInfo.Response) {
        print(info.account_type.priceswrapper)

        title.text = info.account_type.account_type

        for timePrice in info.account_type.priceswrapper {
            let timeLabel = UILabel()
            timeLabel.text = timePrice.time
            timeLabel.textAlignment = .left

            let priceLabel = UILabel()
            priceLabel.text = timePrice.price
            priceLabel.textAlignment = .left

            let newStackView = UIStackView()
            newStackView.addArrangedSubview(timeLabel)
            newStackView.addArrangedSubview(priceLabel)
            newStackView.axis = .horizontal
            newStackView.distribution = .fillEqually
            newStackView.alignment = .fill
            timeOneStackView.addArrangedSubview(newStackView)
        }

        type = 1
        timeSecondStackView.isHidden = true
        dailyLabel.isHidden = true
        sundayLabel.isHidden = true
    }

    private func setupBorder() {
        containerView.layer.masksToBounds = false
        containerView.layer.borderColor = UIColors.lightGray.cgColor
        containerView.layer.borderWidth = 1.2
    }

    @IBAction func showDetailsPressed(_ sender: Any) {
        showMoreButton.isSelected = !showMoreButton.isSelected
        buttonArrow.setImage(showMoreButton.isSelected ? UIImage(named: "button-up") : UIImage(named: "button"), for: .normal)
        delegate?.didTapOnMore(self)
        setupElements()
    }

    private func setupElements() {
        if type != 1 {
        dailyLabel.isHidden = !showMoreButton.isSelected
        timeOneStackView.isHidden = !showMoreButton.isSelected
        sundayLabel.isHidden = !showMoreButton.isSelected
        timeSecondStackView.isHidden = !showMoreButton.isSelected
        } else {
            timeOneStackView.isHidden = !showMoreButton.isSelected
        }
    }

    private func append(views: [UIView], on stackView: UIStackView) {
        var newViews = views
        let restToAdd = numberOfColumns - views.count

        if restToAdd != 0 {
            for _ in  0...restToAdd - 1 {
                let newLabel = UILabel()
                newLabel.text = "   "
                newLabel.textAlignment = .left
                newViews.append(newLabel)
            }
        }

        let newStackView = UIStackView(arrangedSubviews: newViews)
        newStackView.axis = .horizontal
        newStackView.distribution = .fillEqually
        newStackView.alignment = .fill
        stackView.addArrangedSubview(newStackView)
    }
}
