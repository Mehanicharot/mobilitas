//
//  LoginViewController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 31/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField: BlueLineUITextField!
    @IBOutlet weak var passwordTextField: BlueLineUITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            AlertManager.shared.showBasicAlert(self, message: "Please fill login data")
            return
        }

        let data = Login.Request(username: email, password: password)
        WebAPIController.shared.loginRequest(data, completion: { [weak self] results in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch results {
                case .success(let newUser):
                    guard let user = newUser else { return }
                    ActiveUser.shared.setBasicUserData(user)
                    ActiveUser.shared.updateUsername(email)
                    ActiveUser.shared.updatePassword(password)
                    let userDataRequest = UserData.Request(userId: user.userId)
                    WebAPIController.shared.userDataRequest(userDataRequest, completion: { [weak self] results in
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            switch results {
                            case .success(let newUserInfo):
                                if let userInfo = newUserInfo {
                                    ActiveUser.shared.setUserData(userInfo)
                                    self.performSegue(withIdentifier: "Home", sender: nil)
                                }
                            case .failure(let error):
                                print(error)
                                WebAPIController.shared.userDataRequest2(userDataRequest, completion: { [weak self] results in
                                DispatchQueue.main.async { [weak self] in
                                    guard let self = self else { return }
                                    switch results {
                                    case .success(let newUserInfo2):
                                        if let userInfo = newUserInfo2 {
                                            let user = UserData.Response(id: userInfo.id,
                                                                         first_name: userInfo.first_name,
                                                                         last_name: userInfo.last_name,
                                                                         email: userInfo.email,
                                                                         address_1: userInfo.address_1,
                                                                         address_2: userInfo.address_2,
                                                                         country: userInfo.country,
                                                                         birthday: userInfo.birthday,
                                                                         phone_number: userInfo.phone_number,
                                                                         saved_locations: [],
                                                                         facts: userInfo.facts)
                                            ActiveUser.shared.setUserData(user)
                                            self.performSegue(withIdentifier: "Home", sender: nil)
                                        }
                                    case .failure(let error):
                                        AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                                    }
                                }

                                })
                            }
                        }

                    })
                case .failure(let error):
                    AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                }
            }
        })
    }
}
