//
//  BlueLineUITextField.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class BlueLineUITextField: UITextField {
    private var bottomLine: CALayer?
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.textColor = UIColor.darkGreen
        self.font = UIFont(name: "Futura", size: 21)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGreen])

        setupBottomLine()
    }

    private func setupBottomLine() {
        if let line = bottomLine {
            line.removeFromSuperlayer()
        }

        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: -5, y: self.frame.height - 2, width: self.frame.width + 10, height: 2)
        bottomLine.backgroundColor = UIColors.darkBlue.cgColor
        self.borderStyle = UITextField.BorderStyle.none
        self.bottomLine = bottomLine
        self.layer.addSublayer(bottomLine)
    }
}
