//
//  BackButton.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 31/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class BackButton: UIButton {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }

    @objc
    func buttonAction(sender: UIButton!) {
        sender.imageView?.parentContainerViewController?.dismiss(animated: true, completion: nil)
    }
}
