//
//  UserStorage.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 17/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

class UserStorage {
    private let userDefaults = UserDefaults.standard

    private(set) var isFirstStorage: Bool = false

    init() {
        isFirstStorage = !retrieveBool(key: .firstRun)

        if isFirstStorage {
            storeBool(value: true, key: .firstRun)
        }
    }

    func storeUsername(_ value: String) {
        storeString(value: value, key: .username)
    }

    func storePassword(_ value: String) {
        storeString(value: value, key: .password)
    }

    func retriveUsername() -> String? {
        return retrieveString(key: .username)
    }

    func retrivePassword() -> String? {
        return retrieveString(key: .password)
    }

    func removeUsername() {
        removeObject(key: .username)
    }

    func removePassword() {
        removeObject(key: .password)
    }

    func removeAllUserData() {
        let userDataKeys: [StorageKey] = [
            .username,
            .password
        ]

        for key in userDataKeys {
            removeObject(key: key)
        }
    }

    private func storeString(value: String, key: StorageKey) {
        userDefaults.set(value, forKey: key.key)
    }

    private func storeBool(value: Bool, key: StorageKey) {
        userDefaults.set(value, forKey: key.key)
    }

    private func storeDate(value: Date, key: StorageKey) {
        userDefaults.set(value, forKey: key.key)
    }

    private func storeInt(value: Int, key: StorageKey) {
        userDefaults.set(value, forKey: key.key)
    }

    private func retrieveString(key: StorageKey) -> String? {
        return userDefaults.string(forKey: key.key)
    }

    private func retrieveInt(key: StorageKey) -> Int {
        return userDefaults.integer(forKey: key.key)
    }

    private func retrieveBool(key: StorageKey) -> Bool {
        return userDefaults.bool(forKey: key.key)
    }

    private func retriveDate(key: StorageKey) -> Date {
        return userDefaults.object(forKey: key.key) as? Date ?? Date()
    }

    private func removeObject(key: StorageKey) {
        userDefaults.removeObject(forKey: key.key)
    }
}

extension UserStorage {
    struct StorageKey {
        static let firstRun = StorageKey(key: "UserInit", useKeyChain: false)
        static let username = StorageKey(key: "Username", useKeyChain: false)
        static let password = StorageKey(key: "Password", useKeyChain: false)

        private(set) var key: String
        private(set) var useKeyChain: Bool
    }
}
