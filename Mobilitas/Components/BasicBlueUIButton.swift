//
//  BlueUIButton.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol BasicBlueButtonDelegate {
    func didTapOn(_ button: BasicBlueUIButton)
}

class BasicBlueUIButton: UIButton {
    var delegate: BasicBlueButtonDelegate?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.tintColor = .white
        self.titleLabel?.font =  UIFont(name: "Futura", size: 18)
        //setBackgroundColor()
        dropShadow()
    }

    func configure(isGreen: Bool = false) {
        self.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        setBackgroundColor(isGreen ? UIColors.green.cgColor : UIColors.darkBlue.cgColor)
    }

    private func setBackgroundColor(_ color: CGColor) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(isSelected ? color : color)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: .normal)
        }
    }

    private func dropShadow() {
        self.layer.shadowColor = UIColors.darkBlue.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 12.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 4.0
    }

    @objc
    func btnClicked (_ sender: UIButton) {
        delegate?.didTapOn(self)
    }
}
