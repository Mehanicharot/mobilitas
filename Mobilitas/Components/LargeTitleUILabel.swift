//
//  LatgeTitleUILabel.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class LargeTitleUILabel: UILabel {
    private var bottomLayer: CALayer?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.tintColor = UIColors.darkBlue
        self.textColor = UIColors.darkBlue
        self.numberOfLines = 0
        setupBottomLine()
    }

    private func setupBottomLine() {
        if let layer = bottomLayer {
            layer.removeFromSuperlayer()
        }

        let frame = self.frame
        let bottomLayer = CALayer()
        bottomLayer.frame = CGRect(x: 0, y: frame.height + 2, width: frame.width, height: 1.2)
        bottomLayer.backgroundColor = UIColors.lightGray.cgColor
        self.bottomLayer = bottomLayer
        self.layer.addSublayer(bottomLayer)
    }
}
