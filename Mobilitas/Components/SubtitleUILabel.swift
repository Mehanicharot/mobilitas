//
//  SubtitleUILabel.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class SubtitleUILabel: UILabel {
    override func draw(_ rect: CGRect) {
        super.drawText(in: rect)
        self.font = UIFont(name: "Futura Medium", size: 20)
        self.tintColor = UIColor.gray
        self.textColor = UIColor.gray
        self.textAlignment = .left
    }

}
