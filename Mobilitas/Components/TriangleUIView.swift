//
//  TriangleUIView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class TriangleUIView: UIView {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setDownTriangle()
    }

    private func setDownTriangle(){
        let heightWidth = self.frame.size.width
        let path = CGMutablePath()

        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x:heightWidth/2, y: heightWidth/2))
        path.addLine(to: CGPoint(x:heightWidth, y:0))
        path.addLine(to: CGPoint(x:0, y:0))

        let shape = CAShapeLayer()
        shape.path = path
        shape.fillColor = UIColors.darkerBlue.cgColor

        self.layer.insertSublayer(shape, at: 0)
    }

}
