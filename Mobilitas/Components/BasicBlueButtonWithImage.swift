//
//  BasicBlueButtonWithImage.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class BasicBlueButtonWithImage: UIButton {

    override func draw(_ rect: CGRect) {
        self.tintColor = .white
        self.titleLabel?.font =  UIFont(name: "Futura", size: 17)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        setBackgroundColor()
    }

    private func setBackgroundColor() {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(self.isSelected ? UIColors.darkerBlue.cgColor : UIColors.darkBlue.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: .normal)
        }
    }
}
