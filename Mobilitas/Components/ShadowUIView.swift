//
//  ShadowUIView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class ShadowUIView: UIView {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupShadow()
    }

    func setupShadow(_ frame: CGRect? = nil) {
        let shadowPath = UIBezierPath(rect: frame ?? self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = shadowPath.cgPath
    }
}
