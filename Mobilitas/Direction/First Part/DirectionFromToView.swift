//
//  DirectionFromToView.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 30/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

protocol DirectionSearchDelegate {
    func didTouch(_ view: DirectionFromToView, type: SearchType)
    func didTouchOnMyLocation(_ view: DirectionFromToView)
}

enum SearchType {
    case from
    case to
}

class DirectionFromToView: UIView {
    var shouldHideImage: Bool = false
    var type: SearchType = .from

    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!

    var delegate: DirectionSearchDelegate?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        image.isHidden = shouldHideImage
        setupBorder()
    }

    func prepareView(type: SearchType) {
        self.type = type
        self.shouldHideImage = type != .from
    }

    private func setupBorder() {
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColors.lightGray.cgColor
        self.layer.borderWidth = 1.2
    }

    @IBAction func searchPressed(_ sender: Any) {
        delegate?.didTouch(self, type: type)
    }

    @IBAction func getLocationPressed(_ sender: Any) {
        delegate?.didTouchOnMyLocation(self)
    }
}
