//
//  HomeViewController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit
import SafariServices
import GoogleMaps
import GooglePlaces
import DateTimePicker
import DatePickerDialog
import SKCountryPicker

class HomeViewController: UIViewController {
    @IBOutlet weak var shadowView: ShadowUIView!
    @IBOutlet weak var transportOptionButton: BasicBlueButtonWithImage!
    @IBOutlet weak var directionButton: BasicBlueButtonWithImage!
    @IBOutlet weak var accountButton: BasicBlueButtonWithImage!
    @IBOutlet weak var CoreContainerStackView: UIStackView!
    @IBOutlet weak var triangleCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var triangleView: TriangleUIView!
    @IBOutlet weak var categoryTitleLabel: LargeTitleUILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var logoutTitleLabel: UILabel!


    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    var flowCoordinator = ApplicationFlowCoordinator()

    enum CategoryView {
        case direction
        case transportOption
        case account
    }

    enum SubcategoryView {
        case inStepOneOfDirection
        case inStepTwoOfDirection
        case inStepOneOfTransportOption
        case inStepTwoOfTransportOption
        case inStepThreeOfTrnsportOption
        case inStepOneOfAccount
        case inStepTwoOfAccount
    }

    struct TrianglePosition {
        private(set) var left: CGFloat
        private(set) var center: CGFloat
        private(set) var right: CGFloat

        init(width: CGFloat) {
            left = -(width / 3)
            center = CGFloat(0.0)
            right = width / 3
        }
    }

    var activeSearchType: SearchType = .from
    var fromDestination: String? {
        didSet {
            self.removeActiveStackViewElements()
            self.prepareDirectionTab()
        }
    }
    var toDestination: String? {
        didSet {
            self.removeActiveStackViewElements()
            self.prepareDirectionTab()
        }
    }

    var fromMyLocation: LocationInfo? {
        didSet {
            removeActiveStackViewElements()
            self.prepareDirectionTab()
        }
    }

    private var trianglePosition: TrianglePosition!
    private var chooseButtonView: ChooseTimeButtonView?

    var previuosActiveCategory: CategoryView = .direction
    var activeSubcategory: SubcategoryView?
    var activeCategory: CategoryView = .direction {
        didSet {
            deselectTabBar()
            logoutButton.isHidden = true
            logoutTitleLabel.isHidden = true
            switch self.activeCategory {
            case .direction:
                directionButton.isSelected = true
                prepareDirectionTab()
            case .transportOption:
                transportOptionButton.isSelected = true
                prepareTransportOption()
            case .account:
                accountButton.isSelected = true
                prepareAccount()
            }
        }
    }

    private var choosedTime: Int = Int(Date().timeIntervalSince1970)
    private var viewsInCategory: [CategoryView: [UIView]] = [:]
    private var viewsInSubcategory: [SubcategoryView: [UIView]] = [:]
    private var selectedLeg: GoogleMapsDestination.Response.GeneralRouteInfo?

    override func viewDidLoad() {
        super.viewDidLoad()
        trianglePosition = TrianglePosition(width: view.frame.width)
        triangleCenterXConstraint.constant = trianglePosition.left
        activeCategory = .direction
        flowCoordinator.activePart = .direction
        flowCoordinator.delegate = self

        googleMapView.isHidden = true
    }

    @IBAction func directionPressed(_ sender: Any) {
        triangleCenterXConstraint.constant = trianglePosition.left
        activeCategory = .direction

        flowCoordinator.activePart = .direction
    }

    @IBAction func transportOptionPressed(_ sender: Any) {
        triangleCenterXConstraint.constant = trianglePosition.center
        activeCategory = .transportOption

        flowCoordinator.activePart = .transportOptions
    }

    @IBAction func accountPressed(_ sender: Any) {
        triangleCenterXConstraint.constant = trianglePosition.right
        activeCategory = .account

        flowCoordinator.activePart = .account
    }

    private func deselectTabBar() {
        directionButton.isSelected = false
        transportOptionButton.isSelected = false
        accountButton.isSelected = false
        scrollViewBottomConstraint.constant = 0
        googleMapView.isHidden = true
    }

    func prepareDirectionTab() {
        backButton.isHidden = true
//        hideActiveStackViewElements()
        if let views = viewsInCategory[previuosActiveCategory] {
            for view in views {
                view.removeFromSuperview()
            }
        }

        if let sub = activeSubcategory, let views = viewsInSubcategory[sub] {
          for view in views {
              view.removeFromSuperview()
          }
        }

        let fromLabel = UILabel()
        fromLabel.text  = "From"
        fromLabel.textColor = UIColors.darkGray

        let fromView: DirectionFromToView = .fromNib()
        fromView.prepareView(type: .from)
        fromView.delegate = self
        fromView.text.text = fromDestination ?? fromMyLocation?.name ?? ""

        if fromDestination == nil && fromMyLocation == nil {
            fromView.text.text = "Choose destination"
            fromView.text.textColor = UIColor.lightGreen
        } else {
            fromView.text.textColor = UIColor.darkGreen
        }

        let toLabel: ToLabelView = .fromNib()
        toLabel.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        toLabel.delegate = self

        let toView: DirectionFromToView = .fromNib()
        toView.prepareView(type: .to)
        toView.delegate = self
        toView.text.text = toDestination ?? ""

        if toDestination == nil {
            toView.text.text = "Choose destination"
            toView.text.textColor = .lightGreen
        } else {
            toView.text.textColor = UIColor.darkGreen
        }

        let departureLabel = SubtitleUILabel()
        departureLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        departureLabel.text  = "Departure"
        departureLabel.tintColor = UIColors.darkGray
        departureLabel.textColor = UIColors.darkGray

        let getDirectionButton = BasicBlueUIButton()
        getDirectionButton.setTitle("GET DIRECTIONS", for: .normal)
        getDirectionButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        getDirectionButton.configure(isGreen: true)
        getDirectionButton.delegate = self
        //setBackgroundColor(getDirectionButton)

        let chooseTimeButtonView: ChooseTimeButtonView = .fromNib()
        chooseTimeButtonView.delegate = self

        var views: [UIView] = []

        CoreContainerStackView.addArrangedSubview(fromLabel)
        CoreContainerStackView.addArrangedSubview(fromView)
        CoreContainerStackView.addArrangedSubview(toLabel)
        CoreContainerStackView.addArrangedSubview(toView)
        CoreContainerStackView.addArrangedSubview(departureLabel)
        CoreContainerStackView.addArrangedSubview(chooseTimeButtonView)
        CoreContainerStackView.addArrangedSubview(getDirectionButton)


        views.append(fromLabel)
        views.append(fromView)
        views.append(toLabel)
        views.append(toView)
        views.append(departureLabel)
        views.append(getDirectionButton)
        views.append(chooseTimeButtonView)

        viewsInCategory[.direction] = views

        categoryTitleLabel.text = "Plan your Journey"
        previuosActiveCategory = .direction
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }


    func prepareDirectionStep() {
        self.categoryTitleLabel.text = "Plan your Journey"
        var views: [UIView] = []
        let infoView: BikeStationInfoView = .fromNib()
        infoView.prepare(with: fromDestination ?? fromMyLocation?.name ?? "", destination2: toDestination ?? "", shouldUseYellowImage: false)

        backButton.isHidden = false
        CoreContainerStackView.addArrangedSubview(infoView)
        views.append(infoView)

        //TODO

        if fromDestination == nil && fromMyLocation == nil { return }

        guard let from = fromDestination ?? fromMyLocation?.name,
            let to = toDestination else { return }

        WebAPIController.shared.loadTransitRoutes(from: from, to: to, date: choosedTime) { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let newDestinations):
                    guard let destinations = newDestinations else { return }
                    if destinations.routes.isEmpty {
                        AlertManager.shared.showBasicAlert(self, message: "Transit Routes not found")
                    } else {
                    ActiveUser.shared.destinations = destinations
                        if let routes = ActiveUser.shared.destinations?.routes {
                            for route in routes {
                                let routeView: BasicCellWithArrow = .fromNib()
                                routeView.prepareView(for: route)
                                routeView.delegate = self
                                self.CoreContainerStackView.addArrangedSubview(routeView)
                                views.append(routeView)
                            }
                        }
                    }

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                        self.shadowView.setupShadow()
                    })
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }

                self.viewsInSubcategory[.inStepOneOfDirection] = views
                self.activeSubcategory = .inStepOneOfDirection
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareRouteDetails() {
        hideActiveStackViewElements()
        self.categoryTitleLabel.text = "Directions"
        var views: [UIView] = []
        let infoView: BikeStationInfoView = .fromNib()
        infoView.prepare(with: fromDestination ?? fromMyLocation?.name ?? "", destination2: toDestination ?? "", shouldUseYellowImage: false)
        backButton.isHidden = false

        guard let leg = selectedLeg else { return }

        let routeDetails: RouteDetailsView = .fromNib()
        routeDetails.prepare(with: leg)
        routeDetails.delegate = self

        views.append(infoView)
        views.append(routeDetails)

        for (index, step) in leg.steps.enumerated() {
            if step.travel_mode == "TRANSIT"{
                var shouldHide = false
                if index < leg.steps.count - 1 {
                    shouldHide = leg.steps[index + 1].travel_mode == "TRANSIT"
                }

                let busView: BusTransportView = .fromNib()
                busView.prepare(with: step, shouldHideBottomCircel: shouldHide)
                views.append(busView)
            } else if step.travel_mode == "WALKING" {
                let walkingView: WalkingTransportView = .fromNib()
                var fromWalkingInfo = ""
                var toWalkingInfo = ""

                if index != 0 && index != leg.steps.count - 1 {
                    fromWalkingInfo = leg.steps[index-1].transit_details?.arrival_stop.name ?? ""
                    toWalkingInfo = leg.steps[index+1].transit_details?.departure_stop.name ?? ""
                } else if index == 0 && leg.steps.count > 1 {
                    fromWalkingInfo = fromDestination ?? fromMyLocation?.name ?? ""
                    toWalkingInfo = leg.steps[index+1].transit_details?.departure_stop.name ?? ""
                } else if index == leg.steps.count - 1 && index != 0 {
                    fromWalkingInfo = leg.steps[index-1].transit_details?.arrival_stop.name ?? ""
                    toWalkingInfo = toDestination ?? ""
                }
                let displayCircle = index == leg.steps.count - 1
                let generalFrom = self.fromDestination ?? fromMyLocation?.name ?? ""
                let generalTo = self.toDestination
                walkingView.prepare(with: step, shouldDisplayTopCircle: index == 0, shouldDisplayBottomCircle: displayCircle, from: generalFrom, to: generalTo, walkingFromTitle: fromWalkingInfo, wlakingToTitle: toWalkingInfo)
                walkingView.delegate = self
                views.append(walkingView)
            }
        }

        if views.count == 4 {
            let view = UILabel()
            view.text = ""
            views.append(view)
        }

        for view in views {
            self.CoreContainerStackView.addArrangedSubview(view)
        }

        self.viewsInSubcategory[.inStepTwoOfDirection] = views
        self.activeSubcategory = .inStepTwoOfDirection

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareWalkingMap(step: GoogleMapsDestination.Response.Step?, from: String, to: String) {
        categoryTitleLabel.text = "Walking"
        guard let step = step else { return }
        googleMapView.isHidden = false

        var views: [UIView] = []
        let infoView: BikeStationInfoView = .fromNib()
        infoView.prepare(with: from, destination2: to)
        infoView.heightAnchor.constraint(equalToConstant: 70.0).isActive = true

        CoreContainerStackView.addArrangedSubview(infoView)
        views.append(infoView)

        viewsInSubcategory[.inStepThreeOfTrnsportOption] = views
        activeSubcategory = .inStepThreeOfTrnsportOption

        let camera = GMSCameraPosition.camera(withLatitude: step.end_location.lat, longitude: step.end_location.lng, zoom: Float(16))
        googleMapView.camera = camera


        if let polyline = step.polyline?.points {
            set(polyline: GMSPolyline(path: GMSPath(fromEncodedPath: polyline)), on: googleMapView)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
            self.scrollViewBottomConstraint.constant = self.mainScrollView.frame.height - (self.CoreContainerStackView.frame.height * 2) + 20
        })

        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    func drawPath(from polyStr: String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.map = googleMapView
    }

    private func set(polyline: GMSPolyline, on mapView: GMSMapView) {
        guard let path = polyline.path else {
            return
        }
        mapView.clear()
        let intervalDistanceIncrement: CGFloat = 10
        let circleRadiusScale = 1 / mapView.projection.points(forMeters: 1, at: mapView.camera.target)
        var previousCircle: GMSCircle?
        for coordinateIndex in 0 ..< path.count() - 1 {
            let startCoordinate = path.coordinate(at: coordinateIndex)
            let endCoordinate = path.coordinate(at: coordinateIndex + 1)
            let startLocation = CLLocation(latitude: startCoordinate.latitude, longitude: startCoordinate.longitude)
            let endLocation = CLLocation(latitude: endCoordinate.latitude, longitude: endCoordinate.longitude)
            let pathDistance = endLocation.distance(from: startLocation)
            let intervalLatIncrement = (endLocation.coordinate.latitude - startLocation.coordinate.latitude) / pathDistance
            let intervalLngIncrement = (endLocation.coordinate.longitude - startLocation.coordinate.longitude) / pathDistance
            for intervalDistance in 0 ..< Int(pathDistance) {
                let intervalLat = startLocation.coordinate.latitude + (intervalLatIncrement * Double(intervalDistance))
                let intervalLng = startLocation.coordinate.longitude + (intervalLngIncrement * Double(intervalDistance))
                let circleCoordinate = CLLocationCoordinate2D(latitude: intervalLat, longitude: intervalLng)
                if let previousCircle = previousCircle {
                    let circleLocation = CLLocation(latitude: circleCoordinate.latitude,
                                                    longitude: circleCoordinate.longitude)
                    let previousCircleLocation = CLLocation(latitude: previousCircle.position.latitude,
                                                            longitude: previousCircle.position.longitude)
                    if mapView.projection.points(forMeters: circleLocation.distance(from: previousCircleLocation),
                                                 at: mapView.camera.target) < intervalDistanceIncrement {
                        continue
                    }
                }
                let circleRadius = 3 * CLLocationDistance(circleRadiusScale)
                let circle = GMSCircle(position: circleCoordinate, radius: circleRadius)
                circle.fillColor = (intervalDistance == 0 && coordinateIndex == 0) ? UIColors.green : UIColors.darkBlue
                circle.map = mapView
                previousCircle = circle
            }
        }
    }
    func prepareTransportOption() {
        backButton.isHidden = true
        if let views = viewsInCategory[previuosActiveCategory] {
            for view in views {
                view.removeFromSuperview()
            }
        }

        if let sub = activeSubcategory, let views = viewsInSubcategory[sub] {
           for view in views {
               view.removeFromSuperview()
           }
        }

        let fromView: HeaderTextView = .fromNib()
        let ferryView: BasicCellWithArrow = .fromNib()
        ferryView.prepareView(for: .ferry)
        ferryView.delegate = self
        let taxiView: BasicCellWithArrow = .fromNib()
        taxiView.prepareView(for: .taxi)
        taxiView.delegate = self
        let bikeView: BasicCellWithArrow = .fromNib()
        bikeView.prepareView(for: .bike)
        bikeView.delegate = self
        let cycleView: BasicCellWithArrow = .fromNib()
        cycleView.prepareView(for: .cycle)
        cycleView.delegate = self
        let ferryWaterTaxiView: BasicCellWithArrow = .fromNib()
        ferryWaterTaxiView.prepareView(for: .ferryTaxi)
        ferryWaterTaxiView.delegate = self
        let electricCarView: BasicCellWithArrow = .fromNib()
        electricCarView.prepareView(for: .electricCar)
        electricCarView.delegate = self

        var views: [UIView] = []

        CoreContainerStackView.addArrangedSubview(fromView)
        CoreContainerStackView.addArrangedSubview(ferryView)
        CoreContainerStackView.addArrangedSubview(taxiView)
        CoreContainerStackView.addArrangedSubview(bikeView)
        CoreContainerStackView.addArrangedSubview(cycleView)
        CoreContainerStackView.addArrangedSubview(ferryWaterTaxiView)
        CoreContainerStackView.addArrangedSubview(electricCarView)

        views.append(fromView)
        views.append(ferryView)
        views.append(taxiView)
        views.append(bikeView)
        views.append(cycleView)
        views.append(ferryWaterTaxiView)
        views.append(electricCarView)

        categoryTitleLabel.text = "Choose more options"
        viewsInCategory[.transportOption] = views
        previuosActiveCategory = .transportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareAccount() {
        logoutButton.isHidden = !(ActiveUser.shared.userData != nil)
        logoutTitleLabel.isHidden = !(ActiveUser.shared.userData != nil)
        backButton.isHidden = true
        if let views = viewsInCategory[previuosActiveCategory] {
            for view in views {
                view.removeFromSuperview()
            }
        }

        if let sub = activeSubcategory, let views = viewsInSubcategory[sub] {
            for view in views {
                view.removeFromSuperview()
            }
        }

        var views: [UIView] = []

        if let data = ActiveUser.shared.userData {
            let editInfoView: BasicCellWithArrow = .fromNib()
            editInfoView.prepareView(for: .edit)
            editInfoView.delegate = self

            let savedLocationView: BasicCellWithArrow = .fromNib()
            savedLocationView.prepareView(for: .savedLocations)
            savedLocationView.delegate = self

            let profileInfoView: ProfileInfoView = .fromNib()
            profileInfoView.prepare(with: data.facts)
            //profileInfoView.heightAnchor.constraint(equalToConstant: 280.0).isActive = true

            CoreContainerStackView.addArrangedSubview(editInfoView)
            CoreContainerStackView.addArrangedSubview(savedLocationView)
            CoreContainerStackView.addArrangedSubview(profileInfoView)

            views.append(editInfoView)
            views.append(savedLocationView)
            views.append(profileInfoView)
        } else {
            let loginView: BasicCellWithArrow = .fromNib()
            loginView.prepareView(for: .login)
            loginView.delegate = self

            let registerView: BasicCellWithArrow = .fromNib()
            registerView.prepareView(for: .register)
            registerView.delegate = self

            CoreContainerStackView.addArrangedSubview(loginView)
            CoreContainerStackView.addArrangedSubview(registerView)

            views.append(loginView)
            views.append(registerView)
        }

        categoryTitleLabel.text = "My Account"
        previuosActiveCategory = .account
        viewsInCategory[.account] = views
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    @IBAction func backPressed(_ sender: Any) {
        googleMapView.clear()
        scrollViewBottomConstraint.constant = 0
        googleMapView.isHidden = true

        switch activeCategory {
        case .direction:
            let screen = flowCoordinator.directionFlow.popLast()
            prepareDirectionTab(in: screen)
        case .transportOption:
            let screen = flowCoordinator.transportOptionFlow.popLast()
            prepareTransportOptionTab(in: screen)
        case .account:
            let screen = flowCoordinator.accountFlow.popLast()
            prepareAccountTab(in: screen)
        }
    }

    @IBAction func didPressLogoutButton(_ sender: Any) {
        AlertManager.shared.showAlertWithApprove(self, message: "Are you sure you want to logout", completion: { [weak self] shouldLogout in
            guard let self = self else { return }
            if shouldLogout {
                ActiveUser.shared.removeStorageData()
                self.logoutButton.isHidden = true
                self.logoutTitleLabel.isHidden = true
                
                self.removeActiveStackViewElements()
                self.prepareAccount()
            }
        })
    }

    func prepareDirectionTab(in step: DirectionSteps?) {
        guard let _ = step else { return }
        removeActiveStackViewElements()
        switch step {
        case .directionRoutes:
            prepareDirectionTab()
        case .routeDetails:
            prepareDirectionStep()
        case .walkingMap:
            prepareRouteDetails()
        default:
            break
        }
    }

    func prepareFerryWinter() {
        categoryTitleLabel.text = "WINNTER"
        guard let winter = ActiveUser.shared.ferryDetailes?.winter else { return }
        var views: [UIView] = []

        for destination in winter.destinations {
            let newView: DropDownWithDetails = .fromNib()
            newView.prepare(with: destination)
            newView.delegate = self
            CoreContainerStackView.addArrangedSubview(newView)
            views.append(newView)
        }

        viewsInSubcategory[.inStepTwoOfTransportOption] = views
        activeSubcategory = .inStepTwoOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareFerrySummer() {
        categoryTitleLabel.text = "SUMMER"
        guard let summer = ActiveUser.shared.ferryDetailes?.summer else { return }
        var views: [UIView] = []

        for destination in summer.destinations {
            let newView: DropDownWithDetails = .fromNib()
            newView.prepare(with: destination)
            CoreContainerStackView.addArrangedSubview(newView)
            views.append(newView)
        }

        viewsInSubcategory[.inStepTwoOfTransportOption] = views
        activeSubcategory = .inStepTwoOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareFerryPrices() {
        categoryTitleLabel.text = "PRICES"
        guard let prices = ActiveUser.shared.ferryDetailes?.prices else { return }
        var views: [UIView] = []

        for price in prices {
            let newView: DropDownWithDetails = .fromNib()
            newView.prepare(with: price)
            newView.delegate = self
            CoreContainerStackView.addArrangedSubview(newView)
            views.append(newView)
        }

        viewsInSubcategory[.inStepTwoOfTransportOption] = views
        activeSubcategory = .inStepTwoOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareTransportOptionTab(in step: TransportOptionSteps?) {
        removeActiveStackViewElements()
        guard let step = step else { return }
        switch step {
        case .ferry, .taxi, .bike, .cycle:
            print("")
            break
        case .main:
            flowCoordinator.pushToTransportOptions(.main)
            prepareTransportOption()
        case .inFerry, .inTaxi, .inBike, .inCycle:
            prepareTransportOption()
        case .inFerryWinnter, .inFerrySummer, .inFerryPrices:
            prepareFerryStepTwo(detailes: ActiveUser.shared.ferryDetailes)
        case .inFerryData:
            prepareFerry()
        case .inBikePricingInfo, .inBikeStations:
            prepareBikeStep()
        case .inBikeStationDetails:
            prepareBikeStations()
        case .ferryTaxi, .electricCar:
            prepareTransportOption()
        case .inElectricCar:
            break
        case .inSavedLocation: break

        }
    }

    func prepareFerry() {
        guard let routes = ActiveUser.shared.ferryRoutes else { return }
        var views: [UIView] = []
        for route in routes {
            let basicView: BasicCellWithArrow = .fromNib()
            basicView.prepareView(for: .inFerry, route: route)
            basicView.route = route
            basicView.delegate = self
            CoreContainerStackView.addArrangedSubview(basicView)
            views.append(basicView)
        }

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {

            self.shadowView.setupShadow()
        })
    }

    func prepareFerryStepTwo(detailes: FerryRoute.Response?) {
        guard let _ = detailes else { return }
        var views: [UIView] = []

        let winterView: BasicCellWithArrow = .fromNib()
        winterView.prepareView(for: .inFerryWinnter)
        winterView.delegate = self

        let summerView: BasicCellWithArrow = .fromNib()
        summerView.prepareView(for: .inFerrySummer)
        summerView.delegate = self

        let pricesView: BasicCellWithArrow = .fromNib()
        pricesView.prepareView(for: .inFerryPrices)
        pricesView.delegate = self

        CoreContainerStackView.addArrangedSubview(winterView)
        CoreContainerStackView.addArrangedSubview(summerView)
        CoreContainerStackView.addArrangedSubview(pricesView)

        views.append(winterView)
        views.append(summerView)
        views.append(pricesView)

        categoryTitleLabel.text = "Ferry Schedule"

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        activeSubcategory = .inStepOneOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            
            self.shadowView.setupShadow()
        })
    }

    func prepareFerryStepThree() {
        let pickedPart = flowCoordinator.peekFromTransportOptions()
        switch pickedPart {
        case .inFerryWinnter:
            prepareFerryWinter()
        case .inFerrySummer:
            prepareFerrySummer()
        case .inFerryPrices:
            prepareFerryPrices()
        default: break
        }
    }

    func prepareTaxiStep() {
        guard let airportTaxi = ActiveUser.shared.airportTaxi else { return }
        var views: [UIView] = []

        let infoLabel = UILabel()
        infoLabel.text  = ActiveUser.shared.staticText?.taxi_text
        infoLabel.numberOfLines = 0
        infoLabel.font = UIFont(name: "Futura", size: 16)
        infoLabel.tintColor = UIColor.gray
        infoLabel.textColor = UIColor.gray
        infoLabel.textAlignment = .left

        CoreContainerStackView.addArrangedSubview(infoLabel)
        views.append(infoLabel)

        for (index, taxi) in airportTaxi.enumerated() {
            let basicView: AirportTaxiCellView = .fromNib()
            basicView.prepare(with: taxi, hideHeader: index != 0)
            CoreContainerStackView.addArrangedSubview(basicView)
            views.append(basicView)
        }

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        activeSubcategory = .inStepOneOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareBikeStep() {
        categoryTitleLabel.text = "Bike Rentals"
        var views: [UIView] = []

        let pricingInfoView: BasicCellWithArrow = .fromNib()
        pricingInfoView.prepareView(for: .inBikePricingInfo)
        pricingInfoView.delegate = self

        let bikeStationsView: BasicCellWithArrow = .fromNib()
        bikeStationsView.prepareView(for: .inBikeStations)
        bikeStationsView.delegate = self

        let partOne = UILabel()
        partOne.text = ActiveUser.shared.staticText?.bike_saving
        partOne.clipsToBounds = false
        partOne.numberOfLines = 0
        partOne.font = UIFont(name: "Futura", size: 15)
        partOne.tintColor = UIColor.gray
        partOne.textColor = UIColor.gray
        partOne.textAlignment = .left

//        let partTwo = UILabel()
//        partTwo.text = "Bike-sharing is an alternative mode of transport which allows individuals to borrow a bike from a docking station and return it to another docking station belonging to the same system."
//        partTwo.numberOfLines = 0
//        partTwo.font = UIFont(name: "Futura", size: 15)
//        partTwo.tintColor = UIColor.gray
//        partTwo.textColor = UIColor.gray
//        partTwo.textAlignment = .left

        CoreContainerStackView.addArrangedSubview(pricingInfoView)
        CoreContainerStackView.addArrangedSubview(bikeStationsView)
        CoreContainerStackView.addArrangedSubview(partOne)
        //CoreContainerStackView.addArrangedSubview(partTwo)

        views.append(pricingInfoView)
        views.append(bikeStationsView)
        views.append(partOne)
        //views.append(partTwo)

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        activeSubcategory = .inStepOneOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareCycleStep() {
        guard let cycleHire = ActiveUser.shared.cycleHire else { return }
        var views: [UIView] = []

        for cycle in cycleHire {
            let basicView: BasicCellWithArrow = .fromNib()
            basicView.prepareView(for: .inCycle, cycleHire: cycle)
            basicView.delegate = self
            CoreContainerStackView.addArrangedSubview(basicView)
            views.append(basicView)
        }

        let partOne = UILabel()
        partOne.text = ActiveUser.shared.staticText?.cycle_saving
        partOne.numberOfLines = 0
        partOne.clipsToBounds = false
        partOne.font = UIFont(name: "Futura", size: 15)
        partOne.tintColor = UIColor.gray
        partOne.textColor = UIColor.gray
        partOne.textAlignment = .left

        CoreContainerStackView.addArrangedSubview(partOne)
        views.append(partOne)

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        activeSubcategory = .inStepOneOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareAccountTab(in step: AccountSteps?) {
        guard let _ = step else { return }
        switch step {
        case .edit:
            prepareAccount()
        case .changePassword:
            prepareEditView()
        case .savedLocations:
            prepareSavedLocation()
        default:
            break
        }
    }

    func prepareBikeInfo() {
        categoryTitleLabel.text = "Bike Prices"
        guard let infos = ActiveUser.shared.bikeInfo else { return }

        var views: [UIView] = []

        for info in infos {
            let newView: DropDownWithDetails = .fromNib()
            newView.prepare(with: info)
            newView.delegate = self
            CoreContainerStackView.addArrangedSubview(newView)
            views.append(newView)
        }

        viewsInSubcategory[.inStepTwoOfTransportOption] = views
        activeSubcategory = .inStepTwoOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareBikeStations() {
        guard let bikeStations = ActiveUser.shared.bikeStations else { return }
        categoryTitleLabel.text = "Bike Stations"

        var views: [UIView] = []

        for station in bikeStations {
            let stationView: BasicCellWithArrow = .fromNib()
            stationView.prepareView(title: station.title, bikeStation: station)
            stationView.buttonType = .inBikeStationDetails
            stationView.delegate = self

            CoreContainerStackView.addArrangedSubview(stationView)
            views.append(stationView)
        }

        viewsInSubcategory[.inStepTwoOfTransportOption] = views
        activeSubcategory = .inStepTwoOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareFerryTaxi() {
        categoryTitleLabel.text = "Ferry Water Taxi"
        guard let cycleHire = ActiveUser.shared.ferryTaxi else { return }
        var views: [UIView] = []

        for cycle in cycleHire {
            let basicView: BasicCellWithArrow = .fromNib()
            basicView.prepareView(for: .inCycle, cycleHire: cycle)
            basicView.delegate = self
            CoreContainerStackView.addArrangedSubview(basicView)
            views.append(basicView)
        }

//        let partOne = UILabel()
//        partOne.text = ActiveUser.shared.staticText?.ferry_text
//        partOne.numberOfLines = 0
//        partOne.font = UIFont(name: "Futura", size: 15)
//        partOne.tintColor = UIColor.gray
//        partOne.textColor = UIColor.gray
//        partOne.textAlignment = .left
//
//        CoreContainerStackView.addArrangedSubview(partOne)
//        views.append(partOne)

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        activeSubcategory = .inStepOneOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareElectricCar() {
        categoryTitleLabel.text = "Electric Car"
        guard let cycleHire = ActiveUser.shared.electricCar else { return }
        var views: [UIView] = []

        for cycle in cycleHire {
            let basicView: BasicCellWithArrow = .fromNib()
            basicView.prepareView(for: .inCycle, cycleHire: cycle)
            basicView.delegate = self
            CoreContainerStackView.addArrangedSubview(basicView)
            views.append(basicView)
        }

        let partOne = UILabel()
        partOne.text = ActiveUser.shared.staticText?.electric_car_saving
        partOne.numberOfLines = 0
        partOne.clipsToBounds = false
        partOne.font = UIFont(name: "Futura", size: 15)
        partOne.tintColor = UIColor.gray
        partOne.textColor = UIColor.gray
        partOne.textAlignment = .left

        CoreContainerStackView.addArrangedSubview(partOne)
        views.append(partOne)

        viewsInSubcategory[.inStepOneOfTransportOption] = views
        activeSubcategory = .inStepOneOfTransportOption
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }


    func prepareInBikeStationDetails(with bikeStation: BikeStation.Response?) {
        guard let station = bikeStation else { return }
        categoryTitleLabel.text = "Station"

        googleMapView.isHidden = false

        var views: [UIView] = []
        let infoView: BikeStationInfoView = .fromNib()
        infoView.prepare(with: station.title)

        CoreContainerStackView.addArrangedSubview(infoView)
        views.append(infoView)

        viewsInSubcategory[.inStepThreeOfTrnsportOption] = views
        activeSubcategory = .inStepThreeOfTrnsportOption

        let camera = GMSCameraPosition.camera(withLatitude: station.map.lat, longitude: station.map.lng, zoom: Float(station.map.zoom))
        googleMapView.camera = camera

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: station.map.lat, longitude: station.map.lng)
        marker.title = station.map.address
        marker.snippet = station.map.city
        marker.icon = UIImage(named: "marker")
        marker.map = googleMapView

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
            self.scrollViewBottomConstraint.constant = self.mainScrollView.frame.height - (self.CoreContainerStackView.frame.height * 2) + 20
        })
    }

    func removeActiveStackViewElements() {
        if let subcategory = activeSubcategory, let views = viewsInSubcategory[subcategory] {
            for view in views {
                view.removeFromSuperview()
            }
            viewsInSubcategory[subcategory]?.removeAll()
        } else if let views = viewsInCategory[activeCategory] {
            for view in views {
                view.removeFromSuperview()
            }
            viewsInCategory[activeCategory]?.removeAll()
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func hideActiveStackViewElements() {
        if let subcategory = activeSubcategory, let views = viewsInSubcategory[subcategory] {
            for view in views {
                view.removeFromSuperview()
            }
        }

        if let views = viewsInCategory[activeCategory] {
            for view in views {
                view.removeFromSuperview()
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareEditView() {
        hideActiveStackViewElements()

        guard let user = ActiveUser.shared.userData else { return }

        var views: [UIView] = []

        let firstNameView: EditProfileComponentView = .fromNib()
        firstNameView.prepare("First Name:", value: user.first_name)
        views.append(firstNameView)

        let lastNameView: EditProfileComponentView = .fromNib()
        lastNameView.prepare("Last Name:", value: user.last_name)
        views.append(lastNameView)

        let countryView: EditProfileComponentView = .fromNib()
        countryView.prepare("Country:", value: user.country ?? "/")
        countryView.delegate = self
        views.append(countryView)

        let birthdayView: EditProfileComponentView = .fromNib()
        birthdayView.prepare("Birthday:", value: user.birthday ?? "/")
        birthdayView.delegate = self
        views.append(birthdayView)

        let addressView: EditProfileComponentView = .fromNib()
        addressView.prepare("Address:", value: user.address_1 ?? "/")
        views.append(addressView)

        let phoneNumberView: EditProfileComponentView = .fromNib()
        phoneNumberView.prepare("Phone Number:", value: user.phone_number ?? "/")
        views.append(phoneNumberView)

        let changePasswordView: ChangePasswordView = .fromNib()
        changePasswordView.delegate = self
        changePasswordView.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        views.append(changePasswordView)

        for view in views {
            CoreContainerStackView.addArrangedSubview(view)
        }

        let saveButton = BasicBlueUIButton()
        saveButton.setTitle("SAVE", for: .normal)
        saveButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        saveButton.configure()
        saveButton.delegate = self

        views.append(saveButton)
        CoreContainerStackView.addArrangedSubview(saveButton)

        viewsInSubcategory[.inStepOneOfAccount] = views
        activeSubcategory = .inStepOneOfAccount

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareChangePassword() {
        hideActiveStackViewElements()

        var views: [UIView] = []

        let oldPasswordView: EditProfileComponentView = .fromNib()
        oldPasswordView.prepare("Old Password:", value: "")
        oldPasswordView.valueTextField.isSecureTextEntry = true
        views.append(oldPasswordView)

        let newPasswordView: EditProfileComponentView = .fromNib()
        newPasswordView.prepare("New Password:", value: "")
        newPasswordView.valueTextField.isSecureTextEntry = true
        views.append(newPasswordView)

        let confirmNewPasswordView: EditProfileComponentView = .fromNib()
        confirmNewPasswordView.prepare("Confirm New Password:", value: "")
        confirmNewPasswordView.valueTextField.isSecureTextEntry = true
        views.append(confirmNewPasswordView)

        for view in views {
            CoreContainerStackView.addArrangedSubview(view)
        }

        let saveButton = BasicBlueUIButton()
        saveButton.setTitle("CHANGE", for: .normal)
        saveButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        saveButton.configure()
        saveButton.delegate = self

        views.append(saveButton)
        CoreContainerStackView.addArrangedSubview(saveButton)

        viewsInSubcategory[.inStepTwoOfAccount] = views
        activeSubcategory = .inStepTwoOfAccount

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func updateUserInfo() {
        var firstName = ""
        var lastName = ""
        var country = ""
        var birthday = ""
        var address = ""
        var phoneNumber = ""
        for view in CoreContainerStackView.arrangedSubviews {
            if let view = view as? EditProfileComponentView {
                switch view.titleLabel.text {
                case "First Name:":
                    firstName = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                case "Last Name:":
                    lastName = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                case "Country:":
                    country = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                case "Birthday:":
                    if let text = view.valueTextField.text, text.count == 1 {
                        birthday = ""
                    } else {
                        birthday = view.valueTextField.text ?? ""
                    }
                case "Address:":
                    address = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                case "Phone Number:":
                    phoneNumber = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                default: break
                }
            }
        }

        let data = UpdateUserData.Request(id: ActiveUser.shared.basicUserInfo?.userId ?? 0, firstName: firstName, lastName: lastName, country: country, birthday: birthday, address: address, phoneNumber: phoneNumber)

        WebAPIController.shared.updateUserDate(data, completion: { [weak self] results in
                       DispatchQueue.main.async { [weak self] in
                           guard let self = self else { return }
                           switch results {
                           case .success(let newUpdateData):
                            print("\(String(describing: newUpdateData))")
                            guard let newData = newUpdateData else { return }
                            let data = UserData.Response(id: ActiveUser.shared.basicUserInfo?.userId ?? 0,
                                                         first_name: firstName == "" ? "/" : firstName,
                                                         last_name: lastName == "" ? "/" : lastName,
                                                         email: ActiveUser.shared.userData?.email ?? "",
                                                         address_1: address == "" ? "/" : address,
                                                         address_2: "",
                                                         country: country == "" ? "/" : country,
                                                         birthday: birthday == "" ? "/" : birthday,
                                                         phone_number: phoneNumber == "" ? "/" : phoneNumber,
                                                         saved_locations: ActiveUser.shared.userData?.saved_locations ?? [],
                                                         facts: newData.facts)
                                ActiveUser.shared.setUserData(data)
                            AlertManager.shared.showBasicAlert(self, message: "Update successful.")
                           case .failure(let error):
                                AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                           }
                       }

        })
    }

    func prepareSavedLocation() {
        hideActiveStackViewElements()
        guard let locations = ActiveUser.shared.userData?.saved_locations else { return }
        var views: [UIView] = []

        categoryTitleLabel.text = "Saved Locations"

        for location in locations {
            let basicView: BasicCellWithArrow = .fromNib()
            basicView.prepareView(for: location)
            basicView.delegate = self
            basicView.visitDelegate = self
            CoreContainerStackView.addArrangedSubview(basicView)
            views.append(basicView)
        }

        if locations.isEmpty {
            AlertManager.shared.showBasicAlert(self, message: "No saved location.")
        }

        viewsInSubcategory[.inStepOneOfAccount] = views
        activeSubcategory = .inStepOneOfAccount
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func prepareSavedLocationDetails(_ location: UserData.Response.Location) {
        categoryTitleLabel.text = location.title

        googleMapView.isHidden = false

        var views: [UIView] = []
        let infoView: BikeStationInfoView = .fromNib()
        infoView.prepare(with: location.description)

        CoreContainerStackView.addArrangedSubview(infoView)
        views.append(infoView)

        viewsInSubcategory[.inStepTwoOfAccount] = views
        activeSubcategory = .inStepTwoOfAccount

        let camera = GMSCameraPosition.camera(withLatitude: location.location?.lat ?? 0.0, longitude: location.location?.lng ?? 0.0, zoom: Float(location.location?.zoom ?? 16))
        googleMapView.camera = camera

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: location.location?.lat ?? 0.0, longitude: location.location?.lng ?? 0.0)
        marker.snippet = location.description
        marker.icon = UIImage(named: "marker")
        marker.map = googleMapView

        flowCoordinator.pushToAccount(.savedLocations)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
            self.scrollViewBottomConstraint.constant = self.mainScrollView.frame.height - (self.CoreContainerStackView.frame.height * 2) + 20
        })
    }
}

extension HomeViewController: VisitLocationDelegate {
    func didPressOnCheckLocation(_ view: BasicCellWithArrow, location: UserData.Response.Location?) {
        guard let location = location else { return }
        hideActiveStackViewElements()
        self.prepareSavedLocationDetails(location)
    }
}

extension HomeViewController: BasicCellDelegate {
    func didTapOnNextButton(_ view: BasicCellWithArrow, type: AccountSteps) {
        switch type {
        case .edit:
            backButton.isHidden = false
            categoryTitleLabel.text = "My Info"
            flowCoordinator.pushToAccount(.edit)
            prepareEditView()
        case .savedLocations:
            backButton.isHidden = false
            categoryTitleLabel.text = "Saved Locations"
            logoutButton.isHidden = true
            logoutTitleLabel.isHidden = true
            flowCoordinator.pushToAccount(.edit)
            prepareSavedLocation()
        case .main:
            backButton.isHidden = true
        case .login:
            backButton.isHidden = true
            performSegue(withIdentifier: "Login", sender: nil)
        case .register:
            backButton.isHidden = true
            performSegue(withIdentifier: "Register", sender: nil)
        case .changePassword:
            break
        case .savedLocationDetails:
            break
        }
    }

    func didTapOnNextButton(_ view: BasicCellWithArrow, type: TransportOptionSteps) {
        backButton.isHidden = false
        if type != .inCycle {
            hideActiveStackViewElements()
        }

        switch type {
        case .main:
            backButton.isHidden = true
        case .ferry:
            categoryTitleLabel.text = view.title.text
            flowCoordinator.pushToTransportOptions(.inFerry)
            activeSubcategory = .inStepOneOfTransportOption
            prepareFerry()
        case .taxi:
            categoryTitleLabel.text = view.title.text
            flowCoordinator.pushToTransportOptions(.inTaxi)
            activeSubcategory = .inStepOneOfTransportOption
            prepareTaxiStep()
        case .bike:
            categoryTitleLabel.text = view.title.text
            flowCoordinator.pushToTransportOptions(.inBike)
            activeSubcategory = .inStepOneOfTransportOption
            prepareBikeStep()
        case .cycle:
            categoryTitleLabel.text = view.title.text
            flowCoordinator.pushToTransportOptions(.inCycle)
            activeSubcategory = .inStepOneOfTransportOption
            prepareCycleStep()
        case .inFerry:
            flowCoordinator.pushToTransportOptions(.inFerryData)
            WebAPIController.shared.ferryRouteDetailsRequest(id: view.route?.id ?? 0) { [weak self] results in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch results {
                case .success(let routesDetails):
                    ActiveUser.shared.ferryDetailes = routesDetails
                    self.prepareFerryStepTwo(detailes: routesDetails)
                case .failure(let error):
                    AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                }
            }
            }
        case .inBike:
            prepareBikeStep()
        case .inCycle:
            openSafari(link: view.cycleHire?.link)
        case .inFerryData:
            prepareFerryStepTwo(detailes: ActiveUser.shared.ferryDetailes)
        case .inFerryWinnter:
            flowCoordinator.pushToTransportOptions(.inFerryWinnter)
            prepareFerryStepThree()
        case .inFerrySummer:
            flowCoordinator.pushToTransportOptions(.inFerrySummer)
            prepareFerryStepThree()
        case .inFerryPrices:
            flowCoordinator.pushToTransportOptions(.inFerryPrices)
            prepareFerryStepThree()
        case .inTaxi: break
        case .inBikePricingInfo:
            flowCoordinator.pushToTransportOptions(.inBikePricingInfo)
            prepareBikeInfo()
        case .inBikeStations:
            flowCoordinator.pushToTransportOptions(.inBikeStations)
            prepareBikeStations()
        case .inBikeStationDetails:
            flowCoordinator.pushToTransportOptions(.inBikeStationDetails)
            prepareInBikeStationDetails(with: view.bikeSationDetails)
        case .ferryTaxi:
            flowCoordinator.pushToTransportOptions(.ferryTaxi)
            prepareFerryTaxi()
        case .electricCar:
            flowCoordinator.pushToTransportOptions(.electricCar)
            prepareElectricCar()
        case .inElectricCar:
            break
        case .inSavedLocation:
            print("REMOVE SAVED LOCATION RELOAD SAVED LOCATION")
        }
    }

    func didTapOnNextButton(_ view: BasicCellWithArrow) {
        if activeSubcategory == .inStepOneOfAccount {
            guard let location = view.location, let user = ActiveUser.shared.basicUserInfo else { return }
            AlertManager.shared.showAlertWithApprove(self, message: "Are you sure you want to delete saved location?", completion: { approve in
                if approve {
                    guard var locations = ActiveUser.shared.userData?.saved_locations else { return }
                    locations.removeAll(where: { $0.id == view.location?.id })
                    ActiveUser.shared.setLocations(locations)
                    UIView.animate(withDuration: 0.1, animations: {
                         self.view.layoutIfNeeded()
                     })
                    view.removeFromSuperview()
                    UIView.animate(withDuration: 0.1, animations: {
                         self.view.layoutIfNeeded()
                     })
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                        self.shadowView.setupShadow()
                    })
                    AlertManager.shared.showBasicAlert(self, message: "Location succesfully removed.")
                    WebAPIController.shared.removeLocation(userId: user.userId, locationId: location.id, completion: { [weak self] results in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        switch results {
                        case .success(let newUpdateData):
                         print("\(String(describing: newUpdateData))")

                        case .failure(let error):
                             AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                        }
                    }
                    })
                }
            })

        } else {
            selectedLeg = view.leg
            hideActiveStackViewElements()
            flowCoordinator.pushToDirection(.routeDetails)
            prepareRouteDetails()
        }
    }

    func saveNewPassword() {
        var oldPassword = ""
        var newPassword = ""
        var confirmNewPassword = ""
        for view in CoreContainerStackView.arrangedSubviews {
            if let view = view as? EditProfileComponentView {
                switch view.titleLabel.text {
                case "Old Password:":
                    oldPassword = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                case "New Password:":
                    newPassword = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                case "Confirm New Password:":
                    confirmNewPassword = view.valueTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
                default: break
                }
            }
        }

        if oldPassword == ActiveUser.shared.password && newPassword != "" && confirmNewPassword != "" && newPassword == confirmNewPassword {
            let pass = ChangePassword.Request(email: ActiveUser.shared.username ?? "", id: ActiveUser.shared.basicUserInfo?.userId ?? 0, old_pass: oldPassword, new_pass: newPassword, confirm_pass: confirmNewPassword)

            WebAPIController.shared.saveNewPassword(pass, completion: { results in
                DispatchQueue.main.async {
                    switch results {
                    case .success(let result):
                        print("\(String(describing: result))")
                        ActiveUser.shared.updatePassword(pass.new_pass)
                    case .failure(let error):
                        AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                    }
                }
            })
        } else {
            AlertManager.shared.showBasicAlert(self, message: "New Password could not be saved.")
        }
    }
}

extension HomeViewController: SFSafariViewControllerDelegate {
    func openSafari(link: String?) {
        guard let link = link, let url = URL(string: link) else { return }
        let safariVC = SFSafariViewController(url: url)
        self.present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }

    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController: FlowCoordinatorDelegate {
    func didChangeStepInDirection(_ stack: [DirectionSteps]) {
        hideActiveStackViewElements()
    }

    func didChangeStepInTransport(_ stack: [TransportOptionSteps]) {

        hideActiveStackViewElements()
    }

    func didChangeStepInAccount(_ stack: [AccountSteps]) {
        hideActiveStackViewElements()
    }
}

extension HomeViewController: BasicBlueButtonDelegate {
    func didTapOn(_ button: BasicBlueUIButton) {
        if flowCoordinator.peekFromAccount() == .changePassword && activeCategory == .account {

            saveNewPassword()
            //WebAPIController.shared.changePassword()
            let _ = flowCoordinator.popFromAccount()
            prepareEditView()
        } else if flowCoordinator.peekFromAccount() == .edit && activeCategory == .account {
            updateUserInfo()
        } else if (fromDestination == nil || fromMyLocation == nil) && toDestination == nil {
            AlertManager.shared.showBasicAlert(self, message: "Please choose destintinations")
        } else {
            flowCoordinator.pushToDirection(.directionRoutes)
            hideActiveStackViewElements()
            prepareDirectionStep()
        }
    }
}

extension HomeViewController: DirectionSearchDelegate {
    func didTouchOnMyLocation(_ view: DirectionFromToView) {
        fromMyLocation = ActiveUser.shared.myLocation
    }

    func didTouch(_ view: DirectionFromToView, type: SearchType) {
        guard let controller = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(identifier: "SearchIdentifier") as? SearchLovationViewController else { return }
        controller.delegate = self
        controller.activeSearchType = type
        present(controller, animated: true, completion: nil)
    }
}

extension HomeViewController: SelectPlaceDelegate {
    func didSelectPlace(_ view: SearchLovationViewController, id: String, type: SearchType) {
        switch type {
        case .from:
            self.fromDestination = id
        case .to:
            self.toDestination = id
        }
    }
}

extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        viewController.dismiss(animated: true, completion: nil)
//        switch activeSearchType {
//        case .from:
//            fromDestination = place
//        case .to:
//            toDestination = place
//        }
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true, completion: nil)
        print(error)
    }

    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController: TimeButtonDelegate {
    func didTapOnNowButton(_ view: ChooseTimeButtonView) {
        choosedTime = Int(Date().timeIntervalSince1970)
    }

    func didTapOnChooseTimeButton(_ view: ChooseTimeButtonView) {
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        picker.selectedDate = Date()
        picker.darkColor = UIColors.green
        picker.doneBackgroundColor = UIColors.green
        picker.highlightColor = UIColors.green
        picker.delegate = self
        picker.show()

        self.chooseButtonView = view
    }
}

extension HomeViewController: DateTimePickerDelegate {
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy HH:mm"
        choosedTime = Int(didSelectDate.timeIntervalSince1970)
        chooseButtonView?.chooseTimeButton.setTitle(dateFormatterGet.string(from: didSelectDate), for: .normal)
    }
}

extension HomeViewController: WalkingViewDelegate {
    func didTapOnWalk(_ view: WalkingTransportView, step: GoogleMapsDestination.Response.Step?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }

    func didTapOnLocation(_ view: WalkingTransportView, step: GoogleMapsDestination.Response.Step?, from: String, to: String) {
        hideActiveStackViewElements()
        flowCoordinator.pushToDirection(.walkingMap)
        prepareWalkingMap(step: step, from: from, to: to)
    }
}

extension HomeViewController: CalendarTextFieldDelegate {
    func didTap(_ view: EditProfileComponentView) {
        if view.titleLabel.text == "Country:" {
            openCountryPicker(view)
        } else {
            datePickerTapped()
        }
    }

    func datePickerTapped() {
        let datePicker = DatePickerDialog(textColor: UIColors.green, buttonColor: UIColors.green)
        datePicker.show("Your Birthday", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", maximumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/MM/yyyy"
                let birthdayView = self.CoreContainerStackView.arrangedSubviews.first(where: {
                    if let view = $0 as? EditProfileComponentView {
                        return view.titleLabel.text == "Birthday:"
                    }
                    return false
                })

                guard let view = birthdayView as? EditProfileComponentView else { return }

                view.valueTextField.text = formatter.string(from: dt)
            }
        }
    }

    func openCountryPicker(_ view: EditProfileComponentView) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in

            view.valueTextField.text = country.countryName
        }

        countryController.detailColor = UIColors.darkBlue
    }
}

extension HomeViewController: DropDownDelegate {
    func didTapOnMore(_ view: DropDownWithDetails) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            self.shadowView.setupShadow()
        })
    }
}

extension HomeViewController: SwitchDirectionsDelegate {
    func switchDirections(_ view: ToLabelView) {
        if let from = fromDestination, let to = toDestination {
            fromDestination = to
            toDestination = from
        }
    }
}

extension HomeViewController: FavoriteRouteDelegate {
    func addToFavorite(_ view: RouteDetailsView, routeDetails: GoogleMapsDestination.Response.GeneralRouteInfo?) {

        view.starIamge.image = UIImage(named: "yellow_star")
        WebAPIController.shared.saveRoute(bustDistance: view.transitDistance, walkingDistance: view.walkingDistance, id: ActiveUser.shared.userData!.id) { _ in
            let userDataRequest = UserData.Request(userId: ActiveUser.shared.userData!.id)
            WebAPIController.shared.userDataRequest(userDataRequest, completion: { results in
                DispatchQueue.main.async {
                    switch results {
                    case .success(let newUserInfo):
                        if let userInfo = newUserInfo {
                            ActiveUser.shared.setUserData(userInfo)
                        }
                    case .failure(let error):
                        print(error)
                        WebAPIController.shared.userDataRequest2(userDataRequest, completion: { results in
                        DispatchQueue.main.async {
                            switch results {
                            case .success(let newUserInfo2):
                                if let userInfo = newUserInfo2 {
                                    let user = UserData.Response(id: userInfo.id,
                                                                 first_name: userInfo.first_name,
                                                                 last_name: userInfo.last_name,
                                                                 email: userInfo.email,
                                                                 address_1: userInfo.address_1,
                                                                 address_2: userInfo.address_2,
                                                                 country: userInfo.country,
                                                                 birthday: userInfo.birthday,
                                                                 phone_number: userInfo.phone_number,
                                                                 saved_locations: [],
                                                                 facts: userInfo.facts)
                                    ActiveUser.shared.setUserData(user)
                                }
                            case .failure(let error):
                                print(error)
                            }
                        }

                        })
                    }
                }

            })
        }

    }
}

extension HomeViewController: ChangePasswordDelegate {
    func didTapForOpen(_ view: ChangePasswordView) {
        flowCoordinator.accountFlow.append(.changePassword)
        prepareChangePassword()
    }

    func didSaveNewPassword() {

    }
}

extension UIColor {
    static let darkGreen = UIColor(hexString: "#6dae59")
    static let lightGreen = UIColor(hexString: "#92c283")
}
