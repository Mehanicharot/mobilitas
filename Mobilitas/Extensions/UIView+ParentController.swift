//
//  UIView+ParentController.swift
//  Mobilitas
//
//  Created by Martin Andonovski on 18.3.21.
//  Copyright © 2021 Martin . Andonovski. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
