//
//  AlertManager.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit

class AlertManager {
    static let shared = AlertManager()

    private init() { }

    func showBasicAlert(_ viewController: UIViewController, with title: String = "Alert", message: String, okTitle: String = "Ok") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(okAction)
        viewController.present(alert, animated: true, completion: nil)
    }

    func showAlertWtihSegue(_ viewController: UIViewController, segueIdentifier: String, with title: String = "Alert", message: String, okTitle: String = "Ok") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: {
                viewController.performSegue(withIdentifier: segueIdentifier, sender: nil)
            })
        })
        alert.addAction(okAction)
        viewController.present(alert, animated: true, completion: nil)
    }

    func showAlertWithApprove(_ viewController: UIViewController, with title: String = "Alert", message: String, okTitle: String = "Ok", completion: @escaping (Bool) -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .destructive, handler: { _ in
            completion(true)
            alert.dismiss(animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            completion(false)
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
}
