//
//  ApplicationFlowAndSteps.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 01/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

enum DirectionSteps {
    case main
    case directionRoutes
    case routeDetails
    case walkingMap
}

enum TransportOptionSteps {
    case main
    case ferry
    case taxi
    case bike
    case cycle
    case ferryTaxi
    case electricCar
    case inFerry
    case inTaxi
    case inBike
    case inCycle
    case inSavedLocation
    case inBikePricingInfo
    case inBikeStations
    case inBikeStationDetails
    case inElectricCar
    case inFerryData
    case inFerryWinnter
    case inFerrySummer
    case inFerryPrices
}

enum AccountSteps {
    case main
    case edit
    case savedLocations
    case login
    case register
    case changePassword
    case savedLocationDetails
}

protocol FlowCoordinatorDelegate {
    func didChangeStepInDirection(_ stack: [DirectionSteps])
    func didChangeStepInTransport(_ stack: [TransportOptionSteps])
    func didChangeStepInAccount(_ stack: [AccountSteps])
}

struct ApplicationFlowCoordinator {
    var activePart: mainNavigationParts = .direction
    var directionFlow: [DirectionSteps] = [.main] {
        didSet {
            //self.delegate?.didChangeStepInDirection(self.directionFlow)
        }
    }

    var transportOptionFlow: [TransportOptionSteps] = [.main] {
           didSet {
               //self.delegate?.didChangeStepInTransport(self.transportOptionFlow)
           }
       }

    var accountFlow: [AccountSteps] = [.main] {
           didSet {
               //self.delegate?.didChangeStepInAccount(self.accountFlow)
           }
       }

    enum mainNavigationParts {
        case direction
        case transportOptions
        case account
    }

    var delegate: FlowCoordinatorDelegate?

    mutating func pushToDirection(_ element: DirectionSteps) {
        directionFlow.append(element)
    }

    mutating func popFromDirection() -> DirectionSteps? {
        return directionFlow.popLast()
    }

    func peekFromDirection() -> DirectionSteps? {
        guard let topElement = directionFlow.last else { return nil }
        return topElement
    }

    mutating func pushToTransportOptions(_ element: TransportOptionSteps) {
        transportOptionFlow.append(element)
    }

    mutating func popFromTransportOptions() -> TransportOptionSteps? {
        return transportOptionFlow.popLast()
    }

    func peekFromTransportOptions() -> TransportOptionSteps? {
        guard let topElement = transportOptionFlow.last else { return nil }
        return topElement
    }

    mutating func pushToAccount(_ element: AccountSteps) {
        accountFlow.append(element)
    }

    mutating func popFromAccount() -> AccountSteps? {
        return accountFlow.popLast()
    }

    func peekFromAccount() -> AccountSteps? {
        guard let topElement = accountFlow.last else { return nil }
        return topElement
    }
}
