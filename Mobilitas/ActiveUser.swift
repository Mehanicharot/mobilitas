//
//  ActiveUser.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 31/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

class ActiveUser {
    static let shared = ActiveUser()
    private(set) var basicUserInfo: Login.Response?
    private(set) var userData: UserData.Response?
    private let userStorage = UserStorage()

    var ferryRoutes: [Route.Response]?
    var ferryDetailes: FerryRoute.Response?
    var airportTaxi: [AirportTaxi.Response]?
    var cycleHire: [CycleHire.Response]?
    var bikeInfo: [BikeInfo.Response]?
    var bikeStations: [BikeStation.Response]?
    var ferryTaxi: [CycleHire.Response]?
    var electricCar: [CycleHire.Response]?
    var destinations: GoogleMapsDestination.Response?
    var myLocation: LocationInfo?
    var staticText: StaticText.Response?

    var isFirstOpening: Bool {
        userStorage.isFirstStorage
    }

    var username: String? {
        userStorage.retriveUsername()
    }

    var password: String? {
        userStorage.retrivePassword()
    }

    func updateUsername(_ username: String) {
        userStorage.storeUsername(username)
    }

    func updatePassword(_ password: String) {
        userStorage.storePassword(password)
    }

    func removeStorageData() {
        ActiveUser.shared.userData = nil
        ActiveUser.shared.basicUserInfo = nil
        userStorage.removeAllUserData()
    }

    private init() { }

    func setBasicUserData(_ data: Login.Response) {
        self.basicUserInfo = data
    }

    func setUserData(_ data: UserData.Response) {
        self.userData = data
    }

    func setLocations(_ data: [UserData.Response.Location]) {
        self.userData?.updateLocations(locations: data)
    }

    func addLocation(_ data: UserData.Response.Location) {
        self.userData?.addLocations(location: data)
    }
}
