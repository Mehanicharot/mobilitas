//
//  SearchLovationViewController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 23/02/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchLovationViewController: UIViewController {
    @IBOutlet weak var searchLocationTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    var textField: UITextField?
    var resultText: UITextView?
    var fetcher: GMSAutocompleteFetcher?
    var predictions: [GMSAutocompletePrediction] = []

    var selectedPlace: GMSAutocompletePrediction?
    var delegate: SelectPlaceDelegate?
    var activeSearchType: SearchType = .from
    var location: Location.Request?
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        edgesForExtendedLayout = []

        // Set bounds to inner-west Sydney Australia.
        let neBoundsCorner = CLLocationCoordinate2D(latitude: 36.10670843,
                                                    longitude: 14.17951673)
        let swBoundsCorner = CLLocationCoordinate2D(latitude: 35.79208907,
                                                    longitude: 14.59826186)
        let bounds = GMSCoordinateBounds(coordinate: neBoundsCorner,
                                         coordinate: swBoundsCorner)

        // Set up the autocomplete filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment

        // Create a new session token.
        let token: GMSAutocompleteSessionToken = GMSAutocompleteSessionToken.init()

        // Create the fetcher.
        fetcher = GMSAutocompleteFetcher(bounds: bounds, filter: filter)
        fetcher?.delegate = self
        fetcher?.provide(token)

        textField = UITextField(frame: CGRect(x: 5.0, y: 10.0,
                                              width: view.bounds.size.width - 5.0,
                                              height: 64.0))
        textField?.autoresizingMask = .flexibleWidth
        //searchBar.addaddTarget(self, action: #selector(textFieldDidChange(textField:)),
                            // for: .editingChanged)
        let placeholder = NSAttributedString(string: "Type a query...")

        textField?.attributedPlaceholder = placeholder

        resultText = UITextView(frame: CGRect(x: 0, y: 65.0,
                                              width: view.bounds.size.width,
                                              height: view.bounds.size.height - 65.0))
        resultText?.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        resultText?.text = "No Results"
        resultText?.isEditable = false

        //self.view.addSubview(textField!)
        //self.view.addSubview(resultText!)
  }

  @objc func textFieldDidChange(textField: UITextField) {

    }

}

extension SearchLovationViewController: GMSAutocompleteFetcherDelegate {
  func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
    let resultsStr = NSMutableString()
    for prediction in predictions {
      resultsStr.appendFormat("\n Primary text: %@\n", prediction.attributedPrimaryText)
      resultsStr.appendFormat("Place ID: %@\n", prediction.placeID)
    }

    self.predictions = predictions
    searchLocationTableView.reloadData()
  }

  func didFailAutocompleteWithError(_ error: Error) {
    resultText?.text = error.localizedDescription
  }
}

extension SearchLovationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.predictions.count + (ActiveUser.shared.userData?.saved_locations?.count ?? 0)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < ActiveUser.shared.userData?.saved_locations?.count ?? 0 {
            guard let userData = ActiveUser.shared.userData else { return UITableViewCell() }
            let item = userData.saved_locations?[indexPath.row]
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as? PlaceTableViewCell else {
                return UITableViewCell()
            }

            cell.placeNameLabel.text = item?.title ?? ""
            cell.favoriteImageView.image = UIImage(named: "yellow_star")
            cell.favoriteImageView.isHidden = ActiveUser.shared.userData == nil
            cell.favoriteButton.isHidden = ActiveUser.shared.userData == nil
            return cell
        } else {
            let item = predictions[indexPath.row - (ActiveUser.shared.userData?.saved_locations?.count ?? 0)]
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as? PlaceTableViewCell else {
                return UITableViewCell()
            }

            cell.placeNameLabel.text = item.attributedFullText.string
            cell.place = item
            cell.delegate = self
            cell.favoriteImageView.isHidden = ActiveUser.shared.userData == nil
            cell.favoriteButton.isHidden = ActiveUser.shared.userData == nil
            return cell
        }
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < ActiveUser.shared.userData?.saved_locations?.count ?? 0 {
            guard let items = ActiveUser.shared.userData?.saved_locations else { return }
            delegate?.didSelectPlace(self, id: items[indexPath.row].description, type: self.activeSearchType)
        } else {
            let item = predictions[indexPath.row - (ActiveUser.shared.userData?.saved_locations?.count ?? 0)]
            delegate?.didSelectPlace(self, id: item.attributedFullText.string, type: self.activeSearchType)
        }

        self.dismiss(animated: true, completion: nil)
    }
}

protocol SelectPlaceDelegate {
    func didSelectPlace(_ view: SearchLovationViewController, id: String, type: SearchType)
}

extension SearchLovationViewController: FavoriteLocationDelegate {
    func cancelPressed(_ view: FavoriteRouteViewController) {
        self.searchLocationTableView.reloadData()
    }

    func didPickName(_ view: FavoriteRouteViewController, name: String) {
        guard let id = ActiveUser.shared.basicUserInfo?.userId,
            let place = selectedPlace else { return }

        WebAPIController.shared.getPlaceInfo(place.placeID) { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let placeInfo):
                    guard let location = placeInfo?.results?.first?.geometry.location else { return }
                    let locationRequest = Location.Request(id: id,
                                                    address: name,
                                                    lat: location.lat,
                                                    lng: location.lng,
                                                    description: place.attributedFullText.string,
                                                    place_id: place.placeID)

                    WebAPIController.shared.saveLocation(locationRequest, completion: { _ in
                        let dataRequest = UserData.Request(userId: ActiveUser.shared.basicUserInfo?.userId ?? 0)
                        WebAPIController.shared.userDataRequest(dataRequest, completion: { results in
                        DispatchQueue.main.async {
                            switch results {
                            case .success(let newUserInfo):
                                if let userInfo = newUserInfo {
                                    ActiveUser.shared.setUserData(userInfo)
                                    self.searchLocationTableView.reloadData()
                                }
                            case .failure(let error):
                                print(error)
                            }
                        }

                        })
                    })
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
            }
        }

        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
          UInt(GMSPlaceField.placeID.rawValue))!

        let placeClient = GMSPlacesClient()
        placeClient.fetchPlace(fromPlaceID: place.placeID, placeFields: fields, sessionToken: nil, callback: {
          (place: GMSPlace?, error: Error?) in
          if let error = error {
            print("An error occurred: \(error.localizedDescription)")
            return
          }
          if let place = place {
            //self.lblName?.text = place.name
            print("The selected place is: \(place.name)")
          }
        })




    }
}

extension SearchLovationViewController: SaveLocationDelegate {
    func didSetLocation(_ cell: PlaceTableViewCell, place: GMSAutocompletePrediction?) {
        selectedPlace = place
        performSegue(withIdentifier: "SaveLocation", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SaveLocation" {
            if let controller = segue.destination as? FavoriteRouteViewController {
                controller.delegate = self
            }
        }
    }
}

protocol SaveLocationDelegate {
    func didSetLocation(_ cell: PlaceTableViewCell, place: GMSAutocompletePrediction?)
}

class PlaceTableViewCell: UITableViewCell {
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!

    var place: GMSAutocompletePrediction?
    var delegate: SaveLocationDelegate?

    @IBAction func didTapFavorite(_ sender: Any) {
        self.favoriteImageView.image = UIImage(named: "yellow_star")
        delegate?.didSetLocation(self, place: place)
    }

    override func prepareForReuse() {
        self.favoriteImageView.image = UIImage(named: "star_new")
    }
}

extension SearchLovationViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        fetcher?.sourceTextHasChanged(searchBar.text)
    }
}
