//
//  Login.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 29/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

enum Login {
    struct Request: Codable {
        var username: String
        var password: String
    }

    struct Response: Codable {
        private enum CodingKeys: String, CodingKey {
            case token
            case email = "user_email"
            case nickname = "user_nicename"
            case displayName = "user_display_name"
            case userId = "user_id"
        }

        var token: String = ""
        var email: String = ""
        var nickname: String = ""
        var displayName: String = ""
        var userId: Int = 0
    }
}

enum Register {
    struct Request: Codable {
        var first_name: String?
        var last_name: String?
        var email: String?
        var password: String?
    }
    struct test: Codable {
        var username: String
        var password: String
    }

    struct Response: Codable {}
}

enum UserData {
    struct Request: Codable {
        var userId: Int
    }

    struct Response: Codable {
        var id: Int
        var first_name: String
        var last_name: String
        var email: String
        var address_1: String?
        var address_2: String?
        var country: String?
        var birthday: String?
        var phone_number: String?
        var saved_locations: [Location]?
        var facts: Facts

        struct Location: Codable {
            var id: Int
            var title: String
            var description: String
            var location: LocationMoreInfo?
        }

        struct LocationMoreInfo: Codable {
            var lng: Double?
            var lat: Double?
            var zoom: Int?
        }

        mutating func updateLocations(locations: [Location]) {
            self.saved_locations = locations
        }

        mutating func addLocations(location: Location) {
            self.saved_locations?.append(location)
        }
    }

    struct Response2: Codable {
        var id: Int
        var first_name: String
        var last_name: String
        var email: String
        var address_1: String?
        var address_2: String?
        var country: String?
        var birthday: String?
        var phone_number: String?
        var facts: Facts

        struct Location: Codable {
            var id: Int
            var title: String
            var description: String
        }
    }

    struct Facts: Codable {
        var status: Int
        var message: String
        var line_bus_distance: String
        var line_walking_distance: String
        var line_total_saved: String
        var bus_distance: String
        var walking_distance: String
        var total_saved: String
    }
}

enum UpdateUserData {
    struct Request: Codable {
        var id: Int
        var firstName: String
        var lastName: String
        var country: String
        var birthday: String
        var address: String
        var phoneNumber: String
    }
}

enum Location {
    struct Request: Codable {
        var id: Int
        var address: String
        var lat: Double
        var lng: Double
        var description: String
        var place_id: String
        var city = "Birgu"
        var country = "Malta"
        var country_short = "MT"
    }

    struct Response: Codable { }
}

enum ChangePassword {
    struct Request: Codable {
        var email: String
        var id: Int
        var old_pass: String
        var new_pass: String
        var confirm_pass: String
    }

    struct Response: Codable { }
}

enum PlaceInfo {
    struct Request: Codable { }
    struct Response: Codable {
        var results: [ResultDetail]?
        var status: String
    }

    struct ResultDetail: Codable {
        var geometry: Geometry
    }

    struct Geometry: Codable {
        var location: LocationGeometry
    }

    struct LocationGeometry: Codable {
        var lat: Double
        var lng: Double
    }

}
