//
//  WebApiController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

class WebAPIController {
    static let shared = WebAPIController(timeout: 30)

    // MARK: - Private Properties
    private let urlSession: URLSession
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()
    private let baseApiUrl = "https://api.go2green.eu/wp-json/"
    private lazy var loginApi = "\(baseApiUrl)jwt-auth/v1/token"
    private lazy var registerApi = "\(baseApiUrl)services/register-user"
    private lazy var userDataApi = "\(baseApiUrl)services/get-user-info/"
    private lazy var updateUserDataApi = "\(baseApiUrl)services/update-user-info/"
    private lazy var ferryRoutesApi = "\(baseApiUrl)services/ferry-routes/"
    private lazy var ferryRouteApi = "\(baseApiUrl)services/ferry-route/"
    private lazy var taxiInfoApi = "\(baseApiUrl)services/taxi-info"
    private lazy var removeLocation = "\(baseApiUrl)services/remove-location"
    private lazy var saveLocation = "\(baseApiUrl)services/save-location/"
    private lazy var changePassword = "\(baseApiUrl)services/change-password"



    // MARK: - Init
    private init (timeout: TimeInterval = 60) {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = timeout
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        configuration.httpShouldSetCookies = false
        configuration.waitsForConnectivity = false

        urlSession = URLSession(configuration: configuration)
    }

    func loginRequest(_ data: Login.Request, completion: @escaping ((Result<Login.Response?, WebAPIError>) -> Void)) {
        guard let url = URL(string: loginApi) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = try? encoder.encode(data)


        dataTask(with: request, completion: completion)
    }

    func registerRequest(_ data: Register.Request, completion: @escaping ((Result<Register.Response?, WebAPIError>) -> Void)) {
        guard let url = URL(string: registerApi) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = try? encoder.encode(data)

        dataTask(with: request, completion: completion)
    }

    func userDataRequest(_ data: UserData.Request, completion: @escaping ((Result<UserData.Response?, WebAPIError>) -> Void)) {
        let urlString = "\(userDataApi)\(data.userId)"
        guard let url = URL(string: urlString), let key = ActiveUser.shared.basicUserInfo?.token else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func userDataRequest2(_ data: UserData.Request, completion: @escaping ((Result<UserData.Response2?, WebAPIError>) -> Void)) {
        let urlString = "\(userDataApi)\(data.userId)"
        guard let url = URL(string: urlString), let key = ActiveUser.shared.basicUserInfo?.token else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func updateUserDate(_ data: UpdateUserData.Request, completion: @escaping ((Result<UserData.Response?, WebAPIError>) -> Void)) {
        let urlString = "\(updateUserDataApi)"
        guard let url = URL(string: urlString), let key = ActiveUser.shared.basicUserInfo?.token else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = try? encoder.encode(data)

        dataTask(with: request, completion: completion)
    }

    func getText(completion: @escaping ((Result<StaticText.Response?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/get-texts"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func ferryRoutesRequest(completion: @escaping ((Result<[Route.Response]?, WebAPIError>) -> Void)) {
        guard let url = URL(string: ferryRoutesApi) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func ferryRouteDetailsRequest(id: Int, completion: @escaping ((Result<FerryRoute.Response?, WebAPIError>) -> Void)) {
        let urlString = "\(ferryRouteApi)\(id)"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func airportTaxiRequest(completion: @escaping ((Result<[AirportTaxi.Response]?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/taxi-info"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func cycleHireRequest(completion: @escaping ((Result<[CycleHire.Response]?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/cycle-hire"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func bikeInfoRequest(completion: @escaping ((Result<[BikeInfo.Response]?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/bike-info"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func bikeStationsRequest(completion: @escaping ((Result<[BikeStation.Response]?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/bike-stations/"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func ferryTaxiRequest(completion: @escaping ((Result<[CycleHire.Response]?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/ferry-water-taxi"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    func electricCarRequest(completion: @escaping ((Result<[CycleHire.Response]?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/electric-car"
        guard let url = URL(string: urlString) else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue

        dataTask(with: request, completion: completion)
    }

    let googleMapsKey = "AIzaSyBJX1X9TKSkqShf7KL9_HpnrG32ddfOsFE"
    let googleMapsRequestApi = "https://maps.googleapis.com/maps/api/"

    func loadTransitRoutes(from: String, to: String, date: Int, completion: @escaping ((Result<GoogleMapsDestination.Response?, WebAPIError>) -> Void)) {
        var fromLocation: String = ""
        if let myLocation = ActiveUser.shared.myLocation, myLocation.name == from {
            fromLocation = "\(myLocation.latitude),\(myLocation.longitude)"
        } else {
            fromLocation = from
        }

        let urlString = "\(googleMapsRequestApi)directions/json?origin=\(fromLocation)&destination=\(to)&mode=transit&key=\(googleMapsKey)&alternatives=true&departure_time=\(date)"

        if let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encoded) {
            var request = URLRequest(url: url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethod.get.rawValue

            dataTask(with: request, completion: completion)
        }
    }

    func removeLocation(userId: Int, locationId: Int, completion: @escaping ((Result<Register.Response?, WebAPIError>) -> Void)) {
        let urlString = "\(removeLocation)?id=\(userId)&location_id=\(locationId)"
        if let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encoded),
            let key = ActiveUser.shared.basicUserInfo?.token {
            var request = URLRequest(url: url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
            request.httpMethod = HTTPMethod.post.rawValue

            dataTask(with: request, completion: completion)
        }
    }

    func saveLocation(_ data: Location.Request, completion: @escaping ((Result<Location.Response?, WebAPIError>) -> Void)){
        let urlString = saveLocation
        guard let url = URL(string: urlString), let key = ActiveUser.shared.basicUserInfo?.token else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = try? encoder.encode(data)

        dataTask(with: request, completion: completion)
    }

    func saveNewPassword(_ data: ChangePassword.Request, completion: @escaping ((Result<ChangePassword.Response?, WebAPIError>) -> Void)) {
        let urlString = "\(changePassword)?email=\(data.email)&id=\(data.id)&old_pass=\(data.old_pass)&new_pass=\(data.new_pass)&confirm_pass=\(data.confirm_pass)"
        if let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encoded),
            let key = ActiveUser.shared.basicUserInfo?.token {
            var request = URLRequest(url: url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
            request.httpMethod = HTTPMethod.post.rawValue

            dataTask(with: request, completion: completion)
        }
    }

    func getPlaceInfo(_ placeID: String, completion: @escaping ((Result<PlaceInfo.Response?, WebAPIError>) -> Void)) {
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?place_id=\(placeID)&key=\(googleMapsKey)"

        if let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encoded) {
            var request = URLRequest(url: url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethod.get.rawValue

            dataTask(with: request, completion: completion)
        }
    }

    func saveRoute(bustDistance: Double, walkingDistance: Double, id: Int, completion: @escaping ((Result<PlaceInfo.Response?, WebAPIError>) -> Void)) {
        let urlString = "https://mobilitas.fewgoodgeeks.com/wp-json/services/save-route/?bus_distance=\(bustDistance)&walking_distance=\(walkingDistance)&id=\(id)"
        if let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encoded),
            let key = ActiveUser.shared.basicUserInfo?.token {
            var request = URLRequest(url: url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
            request.httpMethod = HTTPMethod.post.rawValue

            dataTask(with: request, completion: completion)
        }
    }

    func searchLocation(with string: String, completion: @escaping ((Result<GoogleMapsDestination.Response, WebAPIError>) -> Void)) {

    }

    private func dataTask<JSONObject: Decodable>(with request: URLRequest, completion: @escaping ((Result<JSONObject?, WebAPIError>) -> Void)) {
        let task = urlSession.dataTask(with: request) { [weak self] data, response, error in
            if let error = error {
                completion(.failure(WebAPIError(error: error as NSError)))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponseReceived))
                return
            }

            guard 200...202 ~= httpResponse.statusCode else {
                completion(.failure(WebAPIError(response: httpResponse, data: data)))
                return
            }

            guard let data = data else {
                completion(.failure(.noDataReceived))
                return
            }

            var object: JSONObject?

            do {
                let test = String(data: data, encoding: .utf8)
                if test?.contains("User Registration Sucessfull.") ?? false {
                    completion(.success(nil))
                    return
                } else if test?.contains("\"Update successful.\"") ?? false {
                    completion(.success(nil))
                    return
                } else if test?.contains("\"Location succesfully removed\"") ?? false {
                    completion(.success(nil))
                } else if test?.contains("No saved locations") ?? false {
                    completion(.success(nil))
                } else if test?.contains("\"Password updated succesfully\"") ?? false {
                    completion(.success(nil))
                }
                object = try self?.decoder.decode(JSONObject.self, from: data)
            } catch {
                print("Decoding Error: \(error)")
            }

            guard let jsonObject = object else {
                completion(.failure(.undecodableJSON))
                return
            }

            completion(.success(jsonObject))
        }

        task.resume()
    }
}

extension String {
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
}


