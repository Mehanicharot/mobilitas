//
//  RegisterViewController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 31/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import UIKit
import UICheckbox_Swift

class RegisterViewController: UIViewController {
    @IBOutlet weak var firstNameTextField: BlueLineUITextField!
    @IBOutlet weak var lastNameTextField: BlueLineUITextField!
    @IBOutlet weak var emailTextField: BlueLineUITextField!
    @IBOutlet weak var passwordTextField: BlueLineUITextField!
    @IBOutlet weak var checkBox: UICheckbox!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func registerPressed(_ sender: UIButton) {
        // TO DO: VALIDATION
        if firstNameTextField.text?.isEmpty ?? false &&
            lastNameTextField.text?.isEmpty ?? false &&
            emailTextField.text?.isEmpty ?? false &&
            passwordTextField.text?.isEmpty ?? false {
            AlertManager.shared.showBasicAlert(self, message: "All fields are required")
        } else {
            if checkBox.isSelected {
                let newUser = Register.Request(first_name: firstNameTextField.text, last_name: lastNameTextField.text, email: emailTextField.text, password: passwordTextField.text)
                WebAPIController.shared.registerRequest(newUser, completion: { results in
                    DispatchQueue.main.async {
                        switch results {
                        case .success(let user):
                            print("\(String(describing: user))")
                            sender.isEnabled = false
                            self.login(sender: sender)
                        case .failure(let error):
                            AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                        }
                    }
                })
                
            } else {
                AlertManager.shared.showBasicAlert(self, message: "Please accept Terms & Conditions")
            }
        }
    }

    private func login(sender: UIButton) {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }

        let data = Login.Request(username: email, password: password)
        WebAPIController.shared.loginRequest(data, completion: { [weak self] results in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch results {
                case .success(let newUser):
                    guard let user = newUser else { return }
                    ActiveUser.shared.setBasicUserData(user)
                    ActiveUser.shared.updateUsername(email)
                    ActiveUser.shared.updatePassword(password)
                    let userDataRequest = UserData.Request(userId: user.userId)
                    WebAPIController.shared.userDataRequest(userDataRequest, completion: { [weak self] results in
                        DispatchQueue.main.async { [weak self] in
                            guard let self = self else { return }
                            switch results {
                            case .success(let newUserInfo):
                                guard let userInfo = newUserInfo else { return }
                                ActiveUser.shared.setUserData(userInfo)
                                self.performSegue(withIdentifier: "Home", sender: nil)
                                sender.isEnabled = true
                            case .failure(let error):
                                print(error)
                                WebAPIController.shared.userDataRequest2(userDataRequest, completion: { [weak self] results in
                                DispatchQueue.main.async { [weak self] in
                                    guard let self = self else { return }
                                    switch results {
                                    case .success(let newUserInfo2):
                                        if let userInfo = newUserInfo2 {
                                            let user = UserData.Response(id: userInfo.id,
                                                                         first_name: userInfo.first_name,
                                                                         last_name: userInfo.last_name,
                                                                         email: userInfo.email,
                                                                         address_1: userInfo.address_1,
                                                                         address_2: userInfo.address_2,
                                                                         country: userInfo.country,
                                                                         birthday: userInfo.birthday,
                                                                         phone_number: userInfo.phone_number,
                                                                         saved_locations: [],
                                                                         facts: userInfo.facts)
                                            ActiveUser.shared.setUserData(user)
                                            self.performSegue(withIdentifier: "Home", sender: nil)
                                            sender.isEnabled = true
                                        }
                                    case .failure(let error):
                                        sender.isEnabled = true
                                        AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                                    }
                                }

                                })
                            }
                        }

                    })
                case .failure(let error):
                    sender.isEnabled = true
                    AlertManager.shared.showBasicAlert(self, message: error.localizedDescription)
                }
            }
        })
    }
    @IBAction func didPressTerms(_ sender: Any) {
        if let url = URL(string: "https://mobilitas.interreg-med.eu/who-we-are/"),
                UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:])
        }
    }
}
