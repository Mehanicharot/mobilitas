//
//  WebAPIError.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

struct APIError: Codable {
    var code: String
    var message: String
}

struct WebAPIError: Error {
    enum Kind {
        case timeout
        case resourceNotFound
        case sessionExpired
        case sessionInvalid
        case badRequest
        case noInternet
        case notHandledHttp
        case notHandledURLError
        case unableToDecodeJSON
        case noDataReceived
        case invalidResponse
    }

    static var resourceNotFound: WebAPIError {
        return WebAPIError(type: .resourceNotFound, code: 404)
    }
    static var invalidSession: WebAPIError {
        return WebAPIError(type: .sessionInvalid, code: 403)
    }
    static var undecodableJSON: WebAPIError {
        return WebAPIError(type: .unableToDecodeJSON, code: 0)
    }
    static var noDataReceived: WebAPIError {
        return WebAPIError(type: .noDataReceived, code: 0)
    }
    static var invalidResponseReceived: WebAPIError {
        return WebAPIError(type: .invalidResponse, code: 0)
    }

    let code: Int
    let type: Kind
    var errorInfo: APIError?

    var localizedDescription: String {
        switch type {
        case .timeout:
            return errorInfo?.message ?? "Our Service is temporarily unavailable. Please try again later."
        case .noInternet:
            return errorInfo?.message ?? "The device is not connected to the internet. Please connect and try again."
        case .badRequest:
            return errorInfo?.message ?? "Incorrect data was attempted to be sent to the server. Please contact the support team."
        case .sessionExpired:
            return errorInfo?.message ?? "Your session has expired. Please log in again."
        case .sessionInvalid:
            return errorInfo?.message ?? "You are not authorized to use this app. Please contact our support team."
        case .resourceNotFound:
            return errorInfo?.message ?? "Unable to connect to the server. Please try again in a few minutes."
        case .unableToDecodeJSON:
            return errorInfo?.message ?? "Unexpected server information received. Please contact the support team."
        case .noDataReceived:
            return errorInfo?.message ?? "No data received from server. Please contact the support team"
        case .invalidResponse:
            return errorInfo?.message ?? "Unexpected response received. Please contact the support team."
        case .notHandledHttp:
            return errorInfo?.message ?? "Request failed with http error: \(code). Please try again in a few minutes."
        case .notHandledURLError:
            return errorInfo?.message ?? "Request failed with error code: \(code). Please try again in a few minutes."
        }
    }

    init (response: HTTPURLResponse, data: Data? = nil) {
        self.code = response.statusCode

        switch response.statusCode {
        case 400:
            self.type = .badRequest
        case 401:
            self.type = .sessionExpired
        case 403:
            self.type = .sessionInvalid
        case 404:
            self.type = .resourceNotFound
        default:
            self.type = .notHandledHttp
        }

        if let data = data {
            errorInfo = try? JSONDecoder().decode(APIError.self, from: data)
        }
    }

    init (error: NSError, data: Data? = nil) {
        self.code = error.code

        switch error.code {
        case NSURLErrorTimedOut:
            self.type = .timeout
        case NSURLErrorNotConnectedToInternet:
            self.type = .noInternet
        default:
            self.type = .notHandledURLError
        }
    }

    private init(type: Kind, code: Int, data: Data? = nil) {
        self.type = type
        self.code = code
    }
}
